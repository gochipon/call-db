//textarea高さ自動調整
(function() {
  jQuery(function($) {
    $("textarea").each(function() {
      var ch = $(this).get(0).scrollHeight;
      var sh = $(this).get(0).offsetHeight;
      var tr_height = ch > sh ? ch : sh ;
      if(tr_height > 0) {
        $(this).animate({"height":tr_height+"px"});
      } else {
        $(this).css("height", "2em");//最低2行程度確保
      }
    });
  });
})(jQuery);