DB_YAML=$CONFIG_DIR/databases.yml

function get_db_host()
{
  grep -v "^#" $DB_YAML | \
    grep -m 1 'host=' | \
    sed -e 's/^.*host=\([^ ]*\) */\1/' | \
    sed -e 's/\([;].*\)//'
}

function get_db_name()
{
  grep -v '^#' $DB_YAML | \
    grep -m 1 'dbname=' | \
    sed -e 's/^.*dbname=*\([^ ]*\) */\1/' | \
    sed -e 's/\([;].*\)//'
}

function get_db_password()
{
  grep -v '^#' $DB_YAML | \
    grep -m 1 'password:' | \
    sed -e 's/^ *password: *\([^ ]*\) */\1/'
}

function get_db_username()
{
  grep -v '^#' $DB_YAML | \
    grep -m 1 'username:' | \
    sed -e 's/^ *username: *\([^ ]*\) */\1/'
}

DB_HOST=$(get_db_host)
DB_NAME=$(get_db_name)
DB_USERNAME=$(get_db_username)
DB_PASSWORD=$(get_db_password)
MYSQL="mysql -u $DB_USERNAME -p$DB_PASSWORD $DB_NAME -h $DB_HOST"
