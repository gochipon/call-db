<?php

/**
 * Store form base class.
 *
 * @method Store getObject() Returns the current form's model object
 *
 * @package    management
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 29553 2010-05-20 14:33:00Z Kris.Wallsmith $
 */
abstract class BaseStoreForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'                     => new sfWidgetFormInputHidden(),
      'region'                 => new sfWidgetFormInputText(),
      'prefecture'             => new sfWidgetFormInputText(),
      'company_name'           => new sfWidgetFormInputText(),
      'company_name_kana'      => new sfWidgetFormInputText(),
      'president'              => new sfWidgetFormInputText(),
      'president_kana'         => new sfWidgetFormInputText(),
      'person_in_charge'       => new sfWidgetFormInputText(),
      'person_in_charge_kana'  => new sfWidgetFormInputText(),
      'sales_representative'   => new sfWidgetFormInputText(),
      'company_address'        => new sfWidgetFormInputText(),
      'company_tel'            => new sfWidgetFormInputText(),
      'company_mobile_phone'   => new sfWidgetFormInputText(),
      'company_fax'            => new sfWidgetFormInputText(),
      'store_name'             => new sfWidgetFormInputText(),
      'store_address'          => new sfWidgetFormInputText(),
      'store_tel'              => new sfWidgetFormInputText(),
      'store_fax'              => new sfWidgetFormInputText(),
      'mail'                   => new sfWidgetFormInputText(),
      'url'                    => new sfWidgetFormInputText(),
      'genre'                  => new sfWidgetFormInputText(),
      'store_count'            => new sfWidgetFormInputText(),
      'new_flag'               => new sfWidgetFormInputCheckbox(),
      'send_document_flag'     => new sfWidgetFormInputCheckbox(),
      'expected_flag'          => new sfWidgetFormInputCheckbox(),
      'appointment_flag'       => new sfWidgetFormInputCheckbox(),
      'call_again_flag'        => new sfWidgetFormInputCheckbox(),
      'failure_receive_flag'   => new sfWidgetFormInputCheckbox(),
      'phone_ban_flag'         => new sfWidgetFormInputCheckbox(),
      'call_schedule'          => new sfWidgetFormInputText(),
      'memo'                   => new sfWidgetFormInputText(),
      'send_document_fax_flag' => new sfWidgetFormInputCheckbox(),
      'created_at'             => new sfWidgetFormDateTime(),
      'updated_at'             => new sfWidgetFormDateTime(),
    ));

    $this->setValidators(array(
      'id'                     => new sfValidatorChoice(array('choices' => array($this->getObject()->get('id')), 'empty_value' => $this->getObject()->get('id'), 'required' => false)),
      'region'                 => new sfValidatorPass(array('required' => false)),
      'prefecture'             => new sfValidatorPass(array('required' => false)),
      'company_name'           => new sfValidatorPass(array('required' => false)),
      'company_name_kana'      => new sfValidatorPass(array('required' => false)),
      'president'              => new sfValidatorPass(array('required' => false)),
      'president_kana'         => new sfValidatorPass(array('required' => false)),
      'person_in_charge'       => new sfValidatorPass(array('required' => false)),
      'person_in_charge_kana'  => new sfValidatorPass(array('required' => false)),
      'sales_representative'   => new sfValidatorPass(array('required' => false)),
      'company_address'        => new sfValidatorPass(array('required' => false)),
      'company_tel'            => new sfValidatorPass(array('required' => false)),
      'company_mobile_phone'   => new sfValidatorPass(array('required' => false)),
      'company_fax'            => new sfValidatorPass(array('required' => false)),
      'store_name'             => new sfValidatorPass(array('required' => false)),
      'store_address'          => new sfValidatorPass(array('required' => false)),
      'store_tel'              => new sfValidatorPass(array('required' => false)),
      'store_fax'              => new sfValidatorPass(array('required' => false)),
      'mail'                   => new sfValidatorPass(array('required' => false)),
      'url'                    => new sfValidatorPass(array('required' => false)),
      'genre'                  => new sfValidatorPass(array('required' => false)),
      'store_count'            => new sfValidatorInteger(array('required' => false)),
      'new_flag'               => new sfValidatorBoolean(array('required' => false)),
      'send_document_flag'     => new sfValidatorBoolean(array('required' => false)),
      'expected_flag'          => new sfValidatorBoolean(array('required' => false)),
      'appointment_flag'       => new sfValidatorBoolean(array('required' => false)),
      'call_again_flag'        => new sfValidatorBoolean(array('required' => false)),
      'failure_receive_flag'   => new sfValidatorBoolean(array('required' => false)),
      'phone_ban_flag'         => new sfValidatorBoolean(array('required' => false)),
      'call_schedule'          => new sfValidatorPass(),
      'memo'                   => new sfValidatorPass(array('required' => false)),
      'send_document_fax_flag' => new sfValidatorBoolean(array('required' => false)),
      'created_at'             => new sfValidatorDateTime(),
      'updated_at'             => new sfValidatorDateTime(),
    ));

    $this->widgetSchema->setNameFormat('store[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'Store';
  }

}
