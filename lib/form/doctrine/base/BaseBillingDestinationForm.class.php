<?php

/**
 * BillingDestination form base class.
 *
 * @method BillingDestination getObject() Returns the current form's model object
 *
 * @package    management
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 29553 2010-05-20 14:33:00Z Kris.Wallsmith $
 */
abstract class BaseBillingDestinationForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'          => new sfWidgetFormInputHidden(),
      'contract_id' => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('Contract'), 'add_empty' => false)),
      'target'      => new sfWidgetFormInputText(),
      'name'        => new sfWidgetFormInputText(),
      'address'     => new sfWidgetFormInputText(),
      'tel'         => new sfWidgetFormInputText(),
      'created_at'  => new sfWidgetFormDateTime(),
      'updated_at'  => new sfWidgetFormDateTime(),
    ));

    $this->setValidators(array(
      'id'          => new sfValidatorChoice(array('choices' => array($this->getObject()->get('id')), 'empty_value' => $this->getObject()->get('id'), 'required' => false)),
      'contract_id' => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('Contract'), 'required' => false)),
      'target'      => new sfValidatorInteger(array('required' => false)),
      'name'        => new sfValidatorPass(array('required' => false)),
      'address'     => new sfValidatorPass(array('required' => false)),
      'tel'         => new sfValidatorPass(array('required' => false)),
      'created_at'  => new sfValidatorDateTime(),
      'updated_at'  => new sfValidatorDateTime(),
    ));

    $this->widgetSchema->setNameFormat('billing_destination[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'BillingDestination';
  }

}
