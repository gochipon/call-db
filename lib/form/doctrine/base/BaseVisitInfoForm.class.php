<?php

/**
 * VisitInfo form base class.
 *
 * @method VisitInfo getObject() Returns the current form's model object
 *
 * @package    management
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 29553 2010-05-20 14:33:00Z Kris.Wallsmith $
 */
abstract class BaseVisitInfoForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'               => new sfWidgetFormInputHidden(),
      'store_id'         => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('Store'), 'add_empty' => false)),
      'visit_date'       => new sfWidgetFormInputText(),
      'target'           => new sfWidgetFormInputText(),
      'status_num'       => new sfWidgetFormInputText(),
      'appointment_type' => new sfWidgetFormInputText(),
      'detail'           => new sfWidgetFormInputText(),
      'created_at'       => new sfWidgetFormDateTime(),
      'updated_at'       => new sfWidgetFormDateTime(),
    ));

    $this->setValidators(array(
      'id'               => new sfValidatorChoice(array('choices' => array($this->getObject()->get('id')), 'empty_value' => $this->getObject()->get('id'), 'required' => false)),
      'store_id'         => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('Store'), 'required' => false)),
      'visit_date'       => new sfValidatorPass(array('required' => false)),
      'target'           => new sfValidatorInteger(),
      'status_num'       => new sfValidatorInteger(array('required' => false)),
      'appointment_type' => new sfValidatorInteger(array('required' => false)),
      'detail'           => new sfValidatorPass(array('required' => false)),
      'created_at'       => new sfValidatorDateTime(),
      'updated_at'       => new sfValidatorDateTime(),
    ));

    $this->widgetSchema->setNameFormat('visit_info[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'VisitInfo';
  }

}
