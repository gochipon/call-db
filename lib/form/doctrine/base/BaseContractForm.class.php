<?php

/**
 * Contract form base class.
 *
 * @method Contract getObject() Returns the current form's model object
 *
 * @package    management
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 29553 2010-05-20 14:33:00Z Kris.Wallsmith $
 */
abstract class BaseContractForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'            => new sfWidgetFormInputHidden(),
      'store_id'      => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('Store'), 'add_empty' => false)),
      'product_id'    => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('Product'), 'add_empty' => false)),
      'contract_date' => new sfWidgetFormDate(),
      'target'        => new sfWidgetFormInputText(),
      'initial_cost'  => new sfWidgetFormInputText(),
      'period_flag'   => new sfWidgetFormInputText(),
      'payment'       => new sfWidgetFormInputText(),
      'start_date'    => new sfWidgetFormDate(),
      'end_date'      => new sfWidgetFormDate(),
      'detail'        => new sfWidgetFormInputText(),
      'created_at'    => new sfWidgetFormDateTime(),
      'updated_at'    => new sfWidgetFormDateTime(),
    ));

    $this->setValidators(array(
      'id'            => new sfValidatorChoice(array('choices' => array($this->getObject()->get('id')), 'empty_value' => $this->getObject()->get('id'), 'required' => false)),
      'store_id'      => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('Store'), 'required' => false)),
      'product_id'    => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('Product'), 'required' => false)),
      'contract_date' => new sfValidatorDate(array('required' => false)),
      'target'        => new sfValidatorInteger(array('required' => false)),
      'initial_cost'  => new sfValidatorInteger(array('required' => false)),
      'period_flag'   => new sfValidatorInteger(array('required' => false)),
      'payment'       => new sfValidatorInteger(array('required' => false)),
      'start_date'    => new sfValidatorDate(array('required' => false)),
      'end_date'      => new sfValidatorDate(array('required' => false)),
      'detail'        => new sfValidatorPass(array('required' => false)),
      'created_at'    => new sfValidatorDateTime(),
      'updated_at'    => new sfValidatorDateTime(),
    ));

    $this->widgetSchema->setNameFormat('contract[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'Contract';
  }

}
