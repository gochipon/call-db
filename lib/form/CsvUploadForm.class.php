<?php
class CsvUploadForm extends sfForm
{
  public function configure()
  {
    // widget
    $this->widgetSchema['csv'] = new sfWidgetFormInputFIle(array(
      'label' => 'アップロード'
    ));
    // label
    $this->widgetSchema->setNameFormat('file[%s]');

    // post validator
    $this->validatorSchema['csv'] = new sfValidatorFile(
      array(
        'required'      => false,
        'path'          => 'uploads/',
        'mime_types'    => array('text/plain'),
      ));
  }
}