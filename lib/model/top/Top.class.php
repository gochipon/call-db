<?php

/**
 * アカウント関連の操作の処理を行う
 * @author Yuki Fukui
 */
class Top
{

  /**
   * アカウントの追加処理
   */
  public static function saveNewAccount($user_info, Doctrine_Connection $conn = null)
  {
    $conn = $conn ? $conn : Doctrine_Manager::connection();
    $conn->beginTransaction();
    try {

      $q = new Account();
      $q->setName($user_info['user_id']);
      $hash_pass = crypt($user_info['user_pass'], '$6$');
      $q->setPassword($hash_pass);
      $q->setAuthorityGroupId($user_info['credential']);
      $q->save($conn);

      $conn->commit();
    } catch (Exception $e) {
      $conn->rollBack();
      throw $e;
    }

  }


  /**
   * パスワードの変更
   */
  public static function changePassword($user_info, Doctrine_Connection $conn = null)
  {
    $conn = $conn ? $conn : Doctrine_Manager::connection();
    $conn->beginTransaction();
    try {

      $q = Mng::getAccount();
      $hash_pass = crypt($user_info['new_pass'], '$6$');
      $q->setPassword($hash_pass);
      $q->save($conn);

      $conn->commit();
    } catch (Exception $e) {
      $conn->rollBack();
      throw $e;
    }

  }

}
