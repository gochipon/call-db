<?php

/**
 * 顧客情報の操作の処理を行う
 * @author Yuki Fukui
 */
class Client
{

  /**
   * 店舗情報の変更処理
   */
  public static function saveStoreChange($post_data, Doctrine_Connection $conn = null)
  {
    $conn = $conn ? $conn : Doctrine_Manager::connection();
    $conn->beginTransaction();
    try {

      $store_record = StoreTable::getInstance()->findOneById($post_data['store_id']);
      if (isset($post_data['call_again_flag'])) {
        $store_record->setCallAgainFlag(1);
      } else {
        $store_record->setCallAgainFlag(0);
      }

      $account_record = Mng::getAccount();
      //制限ユーザーの変更
      if ($account_record['authority_group_id'] == 3) {
        if ($store_record['president'] == '') { $store_record->setPresident($post_data['president']); };
        if ($store_record['president_kana'] == '') { $store_record->setPresidentKana($post_data['president_kana']); };
        if ($store_record['company_address'] == '') { $store_record->setCompanyAddress($post_data['company_address']); };
        if ($store_record['company_fax'] == '') { $store_record->setCompanyFax($post_data['company_fax']); };
        if ($store_record['store_name'] == '') { $store_record->setStoreName($post_data['store_name']); };
        if ($store_record['store_fax'] == '') { $store_record->setStoreFax($post_data['store_fax']); };
        if ($store_record['store_address'] == '') { $store_record->setStoreAddress($post_data['store_address']); };
        if ($store_record['genre'] == '') { $store_record->setGenre($post_data['genre']); };
        if ($store_record['store_count'] == '') { $store_record->setStoreCount($post_data['store_count']); };
        if (isset($post_data['memo'])) {
          if ($store_record['memo'] == '') { $store_record->setMemo($post_data['memo']); };
        }
        if ($store_record['region'] == '') { $store_record->setRegion($post_data['region']); };
        if ($store_record['prefecture'] == '') { $store_record->setPrefecture($post_data['prefecture']); };
      } else {
        $store_record->setPresident($post_data['president']);
        $store_record->setPresidentKana($post_data['president_kana']);
        $store_record->setCompanyAddress($post_data['company_address']);
        $store_record->setCompanyFax($post_data['company_fax']);
        $store_record->setStoreName($post_data['store_name']);
        $store_record->setStoreFax($post_data['store_fax']);
        $store_record->setStoreAddress($post_data['store_address']);
        $store_record->setGenre($post_data['genre']);
        $store_record->setStoreCount($post_data['store_count']);
        $store_record->setMemo($post_data['memo']);
        $store_record->setRegion($post_data['region']);
        $store_record->setPrefecture($post_data['prefecture']);
      }
      $store_record->setCompanyName($post_data['company_name']);
      $store_record->setCompanyNameKana($post_data['company_name_kana']);
      $store_record->setSalesRepresentative($post_data['sales_representative']);
      $store_record->setPersonInChargeKana($post_data['person_in_charge_kana']);
      $store_record->setPersonInCharge($post_data['person_in_charge']);
      $store_record->setCompanyTel($post_data['company_tel']);
      $store_record->setCompanyMobilePhone($post_data['company_mobile_phone']);
      $store_record->setStoreTel($post_data['store_tel']);
      $store_record->setNewFlag($post_data['new_flag']);
      if (isset($post_data['send_document_flag'])) {
        $store_record->setSendDocumentFlag($post_data['send_document_flag']);
      } else {
        $store_record->setSendDocumentFlag(0);
      }
      if (isset($post_data['send_document_fax_flag'])) {
        $store_record->setSendDocumentFaxFlag($post_data['send_document_fax_flag']);
      } else {
        $store_record->setSendDocumentFaxFlag(0);
      }
      if (isset($post_data['expected_flag'])) {
        $store_record->setExpectedFlag($post_data['expected_flag']);
      } else {
        $store_record->setExpectedFlag(0);
      }
      if (isset($post_data['appointment_flag'])) {
        $store_record->setAppointmentFlag($post_data['appointment_flag']);
      } else {
        $store_record->setAppointmentFlag(0);
      }
      if (isset($post_data['failure_receive_flag'])) {
        $store_record->setFailureReceiveFlag($post_data['failure_receive_flag']);
      } else {
        $store_record->setFailureReceiveFlag(0);
      }
      if (isset($post_data['phone_ban_flag'])) {
        $store_record->setPhoneBanFlag($post_data['phone_ban_flag']);
      } else {
        $store_record->setPhoneBanFlag(0);
      }
      $store_record->setCallSchedule($post_data['call_year'] . '-' . $post_data['call_month'] . '-' . $post_data['call_day'] . ' ' . $post_data['call_hour'] . ':' . $post_data['call_min']);
      $store_record->setMail($post_data['mail']);
      $store_record->setUrl($post_data['url']);
      $store_record->save($conn);

      $conn->commit();
    } catch (Exception $e) {
      $conn->rollBack();
      throw $e;
    }

  }


  /**
   * コール履歴の変更を保存
   */
  public static function saveChangeCall($call_info, Doctrine_Connection $conn = null)
  {
    $conn = $conn ? $conn : Doctrine_Manager::connection();
    $conn->beginTransaction();
    try {

      $q = CallInfoTable::getInstance()->findOneById($call_info['id']);
      $q->setCallDate($call_info['call_year'] . '-' . $call_info['call_month'] . '-' . $call_info['call_day'] . ' ' . $call_info['call_hour'] . ':' . $call_info['call_min']);
      $q->setTarget($call_info['call_target']);
      $q->setCaller($call_info['call_caller']);
      $q->setDetail($call_info['call_detail']);
      $q->save($conn);

      $conn->commit();
    } catch (Exception $e) {
      $conn->rollBack();
      throw $e;
    }

  }


  /**
   * コール履歴の追加保存
   */
  public static function saveAddCall($call_info, Doctrine_Connection $conn = null)
  {
    $conn = $conn ? $conn : Doctrine_Manager::connection();
    $conn->beginTransaction();
    try {

      $q = new CallInfo();
      $q->setStoreId($call_info['store_id']);
      $q->setCallDate(date("Y-m-d H:i:s"));
      $q->setTarget($call_info['call_target']);
      if (Mng::getAccount()->getNickname()) {
        $q->setCaller(Mng::getAccount()->getNickname());
      } else {
        $q->setCaller(Mng::getAccount()->getName());
      }
      $q->setDetail($call_info['call_detail']);
      $q->save($conn);

      $conn->commit();
    } catch (Exception $e) {
      $conn->rollBack();
      throw $e;
    }

  }


  /**
   * 訪問履歴の変更を保存
   */
  public static function saveChangeVisit($visit_info, Doctrine_Connection $conn = null)
  {
    $conn = $conn ? $conn : Doctrine_Manager::connection();
    $conn->beginTransaction();
    try {

      $q = VisitInfoTable::getInstance()->findOneById($visit_info['id']);
      $q->setVisitDate($visit_info['visit_year'] . '-' . $visit_info['visit_month'] . '-' . $visit_info['visit_day'] . ' ' . $visit_info['visit_hour'] . ':' . $visit_info['visit_min']);
      $q->setTarget($visit_info['visit_target']);
      $q->setStatusNum($visit_info['status_num']);
      $q->setAppointmentType($visit_info['appointment_type']);
      $q->setDetail($visit_info['visit_detail']);
      $q->save($conn);

      $conn->commit();
    } catch (Exception $e) {
      $conn->rollBack();
      throw $e;
    }

  }


  /**
   * 訪問履歴の追加保存
   */
  public static function saveAddVisit($visit_info, Doctrine_Connection $conn = null)
  {
    $conn = $conn ? $conn : Doctrine_Manager::connection();
    $conn->beginTransaction();
    try {

      $q = new VisitInfo();
      $q->setStoreId($visit_info['store_id']);
      $q->setVisitDate(date("Y-m-d H:i:s"));
      $q->setTarget($visit_info['visit_target']);
      $q->setStatusNum($visit_info['status_num']);
      $q->setAppointmentType($visit_info['appointment_type']);
      $q->setDetail($visit_info['visit_detail']);
      $q->save($conn);

      $conn->commit();
    } catch (Exception $e) {
      $conn->rollBack();
      throw $e;
    }

  }


  /**
   * 提案商材の変更を保存
   */
  public static function saveChangePp($pp, Doctrine_Connection $conn = null)
  {
    $account_record = Mng::getAccount();
    if ($account_record['authority_group_id'] == 3) {
      return;
    }

    $conn = $conn ? $conn : Doctrine_Manager::connection();
    $conn->beginTransaction();
    try {

      if (isset($pp['pp_gochipon_id'])) {
        if (!isset($pp['pp_gochipon']) && $pp['pp_region'] == "" && $pp['pp_prefecture'] == "") {
          ProposalProductTable::getInstance()->findOneById($pp['pp_gochipon_id'])->delete($conn);
        } else {
          $gochipon = ProposalProductTable::getInstance()->findOneById($pp['pp_gochipon_id']);
          $gochipon->setRegion($pp['pp_region']);
          $gochipon->setPrefecture($pp['pp_prefecture']);
          $gochipon->save($conn);
        }
      } else {
        if (isset($pp['pp_gochipon']) || $pp['pp_region'] != "" || $pp['pp_prefecture'] != "") {
          $gochipon = new ProposalProduct();
          $gochipon->setStoreId($pp['store_id']);
          $gochipon->setProductId(1);
          $gochipon->setRegion($pp['pp_region']);
          $gochipon->setPrefecture($pp['pp_prefecture']);
          $gochipon->save($conn);
        }
      }

      if (isset($pp['pp_application_id'])) {
        $application = ProposalProductTable::getInstance()->findOneById($pp['pp_application_id']);
        if (!isset($pp['pp_application']) && !isset($pp['pp_possession_flag'])) {
          $application->delete($conn);
        } else {
          if (isset($pp['pp_possession_flag'])) {
            $application->setPossessionFlag(1);
          } else {
            $application->setPossessionFlag(0);
          }
          $application->save($conn);
        }
      } else {
        if (isset($pp['pp_application']) || isset($pp['pp_possession_flag'])) {
          $application = new ProposalProduct();
          $application->setStoreId($pp['store_id']);
          $application->setProductId(2);
          if (isset($pp['pp_possession_flag'])) {
            $application->setPossessionFlag(1);
          }
          $application->save($conn);
        }
      }

      if (isset($pp['pp_addnet_id'])) {
        if (!isset($pp['pp_addnet'])) {
          ProposalProductTable::getInstance()->findOneById($pp['pp_addnet_id'])->delete($conn);
        }
      } else {
        if (isset($pp['pp_addnet'])) {
          $addnet = new ProposalProduct();
          $addnet->setStoreId($pp['store_id']);
          $addnet->setProductId(3);
          $addnet->save($conn);
        }
      }

      $conn->commit();
    } catch (Exception $e) {
      $conn->rollBack();
      throw $e;
    }

  }


  /**
   * 新規のクライアントを保存する処理
   */
  public static function saveNewClient($post_data, Doctrine_Connection $conn = null)
  {
    $conn = $conn ? $conn : Doctrine_Manager::connection();
    $conn->beginTransaction();
    try {
      //Store新規レコード追加
      $store = new Store();
      $store->setRegion($post_data['region']);
      $store->setPrefecture($post_data['prefecture']);
      $store->setCompanyName($post_data['company_name']);
      $store->setCompanyNameKana($post_data['company_name_kana']);
      $store->setPresident($post_data['president']);
      $store->setPresidentKana($post_data['president_kana']);
      $store->setPersonInCharge($post_data['person_in_charge']);
      $store->setPersonInChargeKana($post_data['person_in_charge_kana']);
      $store->setSalesRepresentative($post_data['sales_representative']);
      $store->setCompanyAddress($post_data['company_address']);
      $store->setCompanyTel($post_data['company_tel']);
      $store->setCompanyMobilePhone($post_data['company_mobile_phone']);
      $store->setCompanyFax($post_data['company_fax']);
      $store->setStoreName($post_data['store_name']);
      $store->setStoreAddress($post_data['store_address']);
      $store->setStoreTel($post_data['store_tel']);
      $store->setStoreFax($post_data['store_fax']);
      $store->setMail($post_data['mail']);
      $store->setUrl($post_data['url']);
      $store->setGenre($post_data['genre']);
      $store->setStoreCount($post_data['store_count']);
      $store->setNewFlag($post_data['new_flag']);
      if(isset($post_data['send_document_flag'])) {
        $store->setSendDocumentFlag($post_data['send_document_flag']);
      }
      if(isset($post_data['send_document_fax_flag'])) {
        $store->setSendDocumentFaxFlag($post_data['send_document_fax_flag']);
      }
      if(isset($post_data['expected_flag'])) {
        $store->setExpectedFlag($post_data['expected_flag']);
      }
      if(isset($post_data['appointment_flag'])) {
        $store->setAppointmentFlag($post_data['appointment_flag']);
      }
      if(isset($post_data['call_again_flag'])) {
        $store->setCallAgainFlag($post_data['call_again_flag']);
      }
      if(isset($post_data['failure_receive_flag'])) {
        $store->setFailureReceiveFlag($post_data['failure_receive_flag']);
      }
      if(isset($post_data['phone_ban_flag'])) {
        $store->setPhoneBanFlag($post_data['phone_ban_flag']);
      }
      if($post_data['call_year'] !== '' && $post_data['call_month'] !== '' && $post_data['call_day'] !== '') {
        $post_data['call_schedule'] = $post_data['call_year'] . '-' . $post_data['call_month'] . '-' . $post_data['call_day'];

        if($post_data['call_hour'] !== '' && $post_data['call_min'] !== '') {
          $post_data['call_schedule'] .= ' ' . $post_data['call_hour'] . ':' . $post_data['call_min'];
        }

        $store->setCallSchedule($post_data['call_schedule']);
      }
      $store->setMemo($post_data['memo']);
      $store->save($conn);


      //提案商材のレコードを追加
      if(isset($post_data['pp_gochipon'])) {
        $pp_gochipon = new ProposalProduct();
        $pp_gochipon->setStoreId($store->getId());
        $pp_gochipon->setProductId(1);
        $pp_gochipon->setRegion($post_data['pp_region']);
        $pp_gochipon->setPrefecture($post_data['pp_prefecture']);
        $pp_gochipon->save($conn);
      }
      if(isset($post_data['pp_application']) || isset($post_data['pp_possession_flag'])) {
        $pp_application = new ProposalProduct();
        $pp_application->setStoreId($store->getId());
        $pp_application->setProductId(2);
        if (isset($post_data['pp_possession_flag'])) {
          $pp_application->setPossessionFlag(1);
        }
        $pp_application->save($conn);
      }
      if(isset($post_data['pp_addnet'])) {
        $pp_addnet = new ProposalProduct();
        $pp_addnet->setStoreId($store->getId());
        $pp_addnet->setProductId(3);
        $pp_addnet->save($conn);
      }


      //コール一覧のレコード追加
      if(isset($post_data['call_target']) || $post_data['call_detail'] !== '') {
        $call_info = new CallInfo();
        $call_info->setStoreId($store->getId());
        $call_info->setCallDate(date("Y-m-d H:i:s"));
        if(isset($post_data['call_target'])) {
          $call_info->setTarget($post_data['call_target']);
        } else {
          //コール対象が選択されておらず詳細が入力されていた場合は自動で企業を指定する
          $call_info->setTarget(0);
        }
        $call_info->setCaller(Mng::getAccount()->getNickname());
        $call_info->setDetail($post_data['call_detail']);
        $call_info->save($conn);
      }


      //訪問一覧のレコード追加
      if(isset($post_data['visit_target']) || $post_data['visit_detail'] !== '') {
        $visit_info = new VisitInfo();
        $visit_info->setStoreId($store->getId());
        $visit_info->setVisitDate(date("Y-m-d H:i:s"));
        if(isset($post_data['visit_target'])) {
          $visit_info->setTarget($post_data['visit_target']);
        } else {
          //訪問対象が選択されておらず詳細が入力されていた場合は自動で企業を指定する
          $visit_info->setTarget(0);
        }
        $visit_info->setStatusNum($post_data['status_num']);
        $visit_info->setAppointmentType($post_data['appointment_type']);
        $visit_info->setDetail($post_data['visit_detail']);
        $visit_info->save($conn);
      }


      //取引商材のレコードを追加
      if(isset($post_data['dp_gochipon'])) {
        $dp_gochipon = new DealProduct();
        $dp_gochipon->setStoreId($store->getId());
        $dp_gochipon->setProductId(1);
        $dp_gochipon->setRegion($post_data['dp_region']);
        $dp_gochipon->setPrefecture($post_data['dp_prefecture']);
        $dp_gochipon->save($conn);
      }
      if(isset($post_data['dp_application'])) {
        $dp_application = new DealProduct();
        $dp_application->setStoreId($store->getId());
        $dp_application->setProductId(2);
        $dp_application->save($conn);
      }
      if(isset($post_data['dp_addnet'])) {
        $dp_addnet = new DealProduct();
        $dp_addnet->setStoreId($store->getId());
        $dp_addnet->setProductId(3);
        $dp_addnet->save($conn);
      }


      //契約内容レコードを追加
      if(isset($post_data['contract_target'])) {

        $contract = new Contract();
        $contract->setStoreId($store->getId());
        $contract->setProductId($post_data['contract_product_name']); 
        if($post_data['contract_year'] !== '' && $post_data['contract_month'] !== '' && $post_data['contract_day'] !== '') {
          $contract->setContractDate($post_data['contract_year'] . '-' . $post_data['contract_month'] . '-' . $post_data['contract_day']);
        }
        $contract->setTarget($post_data['contract_target']);
        $contract->setInitialCost($post_data['initial_cost']);
        if(isset($post_data['period_flag'])) {
          $contract->setPeriodFlag($post_data['period_flag']);
        }
        $contract->setPayment($post_data['payment']);
        if($post_data['start_year'] !== '' && $post_data['start_month'] !== '' && $post_data['start_day'] !== '') {
          $contract->setStartDate($post_data['start_year'] . '-' . $post_data['start_month'] . '-' . $post_data['start_day']);
        }
        if($post_data['end_year'] !== '' && $post_data['end_month'] !== '' && $post_data['end_day'] !== '') {
          $contract->setEndDate($post_data['end_year'] . '-' . $post_data['end_month'] . '-' . $post_data['end_day']);
        }
        $contract->setDetail($post_data['contract_detail']);
        $contract->save($conn);

        //請求書先レコードを追加
        if(isset($post_data['bd_target'])) {
          $bd = new BillingDestination();
          $bd->setContractId($contract->getId());
          $bd->setTarget($post_data['bd_target']);
          $bd->setName($post_data['bd_name']);
          $bd->setAddress($post_data['bd_address']);
          $bd->setTel($post_data['bd_tel']);
          $bd->save($conn);
        }

      }
      $conn->commit();
    } catch (Exception $e) {
      $conn->rollBack();
      throw $e;
    }

  }

  /**
   * storeレコードとそれに付随するレコードの削除
   */
  public static function deleteStoreRecord($store_id, Doctrine_Connection $conn = null)
  {
    $conn = $conn ? $conn : Doctrine_Manager::connection();
    $conn->beginTransaction();
    try {

      VisitInfoTable::getInstance()->findByStoreId($store_id)->delete($conn);
      CallInfoTable::getInstance()->findByStoreId($store_id)->delete($conn);
      DealProductTable::getInstance()->findByStoreId($store_id)->delete($conn);
      ProposalProductTable::getInstance()->findByStoreId($store_id)->delete($conn);

      $contract = ContractTable::getInstance()->findByStoreId($store_id);

      if ($contract->count()) {
        BillingDestinationTable::getInstance()->findByContractId($contract[0]['id'])->delete($conn);
      }

      $contract->delete($conn);

      StoreTable::getInstance()->findOneById($store_id)->delete($conn);

      $conn->commit();
    } catch (Exception $e) {
      $conn->rollBack();
      throw $e;
    }

  }

  /**
   * 訪問履歴のステータス
   */
  private static $status_array = array(
    '0' => '',
    '1' => '受注',
    '2' => '返答待ち',
    '3' => '再訪問',
    '4' => '検討中',
    '5' => '失注'
  );

  public static function getStatusName($status_num)
  {
    return self::$status_array[$status_num];
  }

  /**
   * 商材名の取得
   */
  private static $product_name_array = array(
    '0' => '',
    '1' => 'ごちぽん',
    '2' => 'アプリ',
    '3' => 'アドネットワーク広告'
  );

  public static function getProductName($product_num)
  {
    return self::$product_name_array[$product_num];
  }

}
