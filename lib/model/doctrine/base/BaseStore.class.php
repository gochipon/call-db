<?php

/**
 * BaseStore
 * 
 * This class has been auto-generated by the Doctrine ORM Framework
 * 
 * @property integer $id
 * @property varchar $region
 * @property varchar $prefecture
 * @property varchar $company_name
 * @property varchar $company_name_kana
 * @property varchar $president
 * @property varchar $president_kana
 * @property varchar $person_in_charge
 * @property varchar $person_in_charge_kana
 * @property varchar $sales_representative
 * @property varchar $company_address
 * @property varchar $company_tel
 * @property varchar $company_mobile_phone
 * @property varchar $company_fax
 * @property varchar $store_name
 * @property varchar $store_address
 * @property varchar $store_tel
 * @property varchar $store_fax
 * @property varchar $mail
 * @property varchar $url
 * @property varchar $genre
 * @property integer $store_count
 * @property boolean $new_flag
 * @property boolean $send_document_flag
 * @property boolean $expected_flag
 * @property boolean $appointment_flag
 * @property boolean $call_again_flag
 * @property boolean $failure_receive_flag
 * @property boolean $phone_ban_flag
 * @property datetime $call_schedule
 * @property text $memo
 * @property boolean $send_document_fax_flag
 * @property Doctrine_Collection $CallInfo
 * @property Doctrine_Collection $VisitInfo
 * @property Doctrine_Collection $ProposalProduct
 * @property Doctrine_Collection $DealProduct
 * @property Doctrine_Collection $Contract
 * 
 * @method integer             getId()                     Returns the current record's "id" value
 * @method varchar             getRegion()                 Returns the current record's "region" value
 * @method varchar             getPrefecture()             Returns the current record's "prefecture" value
 * @method varchar             getCompanyName()            Returns the current record's "company_name" value
 * @method varchar             getCompanyNameKana()        Returns the current record's "company_name_kana" value
 * @method varchar             getPresident()              Returns the current record's "president" value
 * @method varchar             getPresidentKana()          Returns the current record's "president_kana" value
 * @method varchar             getPersonInCharge()         Returns the current record's "person_in_charge" value
 * @method varchar             getPersonInChargeKana()     Returns the current record's "person_in_charge_kana" value
 * @method varchar             getSalesRepresentative()    Returns the current record's "sales_representative" value
 * @method varchar             getCompanyAddress()         Returns the current record's "company_address" value
 * @method varchar             getCompanyTel()             Returns the current record's "company_tel" value
 * @method varchar             getCompanyMobilePhone()     Returns the current record's "company_mobile_phone" value
 * @method varchar             getCompanyFax()             Returns the current record's "company_fax" value
 * @method varchar             getStoreName()              Returns the current record's "store_name" value
 * @method varchar             getStoreAddress()           Returns the current record's "store_address" value
 * @method varchar             getStoreTel()               Returns the current record's "store_tel" value
 * @method varchar             getStoreFax()               Returns the current record's "store_fax" value
 * @method varchar             getMail()                   Returns the current record's "mail" value
 * @method varchar             getUrl()                    Returns the current record's "url" value
 * @method varchar             getGenre()                  Returns the current record's "genre" value
 * @method integer             getStoreCount()             Returns the current record's "store_count" value
 * @method boolean             getNewFlag()                Returns the current record's "new_flag" value
 * @method boolean             getSendDocumentFlag()       Returns the current record's "send_document_flag" value
 * @method boolean             getExpectedFlag()           Returns the current record's "expected_flag" value
 * @method boolean             getAppointmentFlag()        Returns the current record's "appointment_flag" value
 * @method boolean             getCallAgainFlag()          Returns the current record's "call_again_flag" value
 * @method boolean             getFailureReceiveFlag()     Returns the current record's "failure_receive_flag" value
 * @method boolean             getPhoneBanFlag()           Returns the current record's "phone_ban_flag" value
 * @method datetime            getCallSchedule()           Returns the current record's "call_schedule" value
 * @method text                getMemo()                   Returns the current record's "memo" value
 * @method boolean             getSendDocumentFaxFlag()    Returns the current record's "send_document_fax_flag" value
 * @method Doctrine_Collection getCallInfo()               Returns the current record's "CallInfo" collection
 * @method Doctrine_Collection getVisitInfo()              Returns the current record's "VisitInfo" collection
 * @method Doctrine_Collection getProposalProduct()        Returns the current record's "ProposalProduct" collection
 * @method Doctrine_Collection getDealProduct()            Returns the current record's "DealProduct" collection
 * @method Doctrine_Collection getContract()               Returns the current record's "Contract" collection
 * @method Store               setId()                     Sets the current record's "id" value
 * @method Store               setRegion()                 Sets the current record's "region" value
 * @method Store               setPrefecture()             Sets the current record's "prefecture" value
 * @method Store               setCompanyName()            Sets the current record's "company_name" value
 * @method Store               setCompanyNameKana()        Sets the current record's "company_name_kana" value
 * @method Store               setPresident()              Sets the current record's "president" value
 * @method Store               setPresidentKana()          Sets the current record's "president_kana" value
 * @method Store               setPersonInCharge()         Sets the current record's "person_in_charge" value
 * @method Store               setPersonInChargeKana()     Sets the current record's "person_in_charge_kana" value
 * @method Store               setSalesRepresentative()    Sets the current record's "sales_representative" value
 * @method Store               setCompanyAddress()         Sets the current record's "company_address" value
 * @method Store               setCompanyTel()             Sets the current record's "company_tel" value
 * @method Store               setCompanyMobilePhone()     Sets the current record's "company_mobile_phone" value
 * @method Store               setCompanyFax()             Sets the current record's "company_fax" value
 * @method Store               setStoreName()              Sets the current record's "store_name" value
 * @method Store               setStoreAddress()           Sets the current record's "store_address" value
 * @method Store               setStoreTel()               Sets the current record's "store_tel" value
 * @method Store               setStoreFax()               Sets the current record's "store_fax" value
 * @method Store               setMail()                   Sets the current record's "mail" value
 * @method Store               setUrl()                    Sets the current record's "url" value
 * @method Store               setGenre()                  Sets the current record's "genre" value
 * @method Store               setStoreCount()             Sets the current record's "store_count" value
 * @method Store               setNewFlag()                Sets the current record's "new_flag" value
 * @method Store               setSendDocumentFlag()       Sets the current record's "send_document_flag" value
 * @method Store               setExpectedFlag()           Sets the current record's "expected_flag" value
 * @method Store               setAppointmentFlag()        Sets the current record's "appointment_flag" value
 * @method Store               setCallAgainFlag()          Sets the current record's "call_again_flag" value
 * @method Store               setFailureReceiveFlag()     Sets the current record's "failure_receive_flag" value
 * @method Store               setPhoneBanFlag()           Sets the current record's "phone_ban_flag" value
 * @method Store               setCallSchedule()           Sets the current record's "call_schedule" value
 * @method Store               setMemo()                   Sets the current record's "memo" value
 * @method Store               setSendDocumentFaxFlag()    Sets the current record's "send_document_fax_flag" value
 * @method Store               setCallInfo()               Sets the current record's "CallInfo" collection
 * @method Store               setVisitInfo()              Sets the current record's "VisitInfo" collection
 * @method Store               setProposalProduct()        Sets the current record's "ProposalProduct" collection
 * @method Store               setDealProduct()            Sets the current record's "DealProduct" collection
 * @method Store               setContract()               Sets the current record's "Contract" collection
 * 
 * @package    management
 * @subpackage model
 * @author     Your name here
 * @version    SVN: $Id: Builder.php 7691 2011-02-04 15:43:29Z jwage $
 */
abstract class BaseStore extends sfDoctrineRecord
{
    public function setTableDefinition()
    {
        $this->setTableName('store');
        $this->hasColumn('id', 'integer', null, array(
             'type' => 'integer',
             'primary' => true,
             'autoincrement' => true,
             'unsigned' => true,
             ));
        $this->hasColumn('region', 'varchar', 255, array(
             'type' => 'varchar',
             'length' => 255,
             ));
        $this->hasColumn('prefecture', 'varchar', 255, array(
             'type' => 'varchar',
             'length' => 255,
             ));
        $this->hasColumn('company_name', 'varchar', 255, array(
             'type' => 'varchar',
             'length' => 255,
             ));
        $this->hasColumn('company_name_kana', 'varchar', 255, array(
             'type' => 'varchar',
             'length' => 255,
             ));
        $this->hasColumn('president', 'varchar', 255, array(
             'type' => 'varchar',
             'length' => 255,
             ));
        $this->hasColumn('president_kana', 'varchar', 255, array(
             'type' => 'varchar',
             'length' => 255,
             ));
        $this->hasColumn('person_in_charge', 'varchar', 255, array(
             'type' => 'varchar',
             'length' => 255,
             ));
        $this->hasColumn('person_in_charge_kana', 'varchar', 255, array(
             'type' => 'varchar',
             'length' => 255,
             ));
        $this->hasColumn('sales_representative', 'varchar', 255, array(
             'type' => 'varchar',
             'length' => 255,
             ));
        $this->hasColumn('company_address', 'varchar', 255, array(
             'type' => 'varchar',
             'length' => 255,
             ));
        $this->hasColumn('company_tel', 'varchar', 255, array(
             'type' => 'varchar',
             'length' => 255,
             ));
        $this->hasColumn('company_mobile_phone', 'varchar', 255, array(
             'type' => 'varchar',
             'length' => 255,
             ));
        $this->hasColumn('company_fax', 'varchar', 255, array(
             'type' => 'varchar',
             'length' => 255,
             ));
        $this->hasColumn('store_name', 'varchar', 255, array(
             'type' => 'varchar',
             'length' => 255,
             ));
        $this->hasColumn('store_address', 'varchar', 255, array(
             'type' => 'varchar',
             'length' => 255,
             ));
        $this->hasColumn('store_tel', 'varchar', 255, array(
             'type' => 'varchar',
             'length' => 255,
             ));
        $this->hasColumn('store_fax', 'varchar', 255, array(
             'type' => 'varchar',
             'length' => 255,
             ));
        $this->hasColumn('mail', 'varchar', 255, array(
             'type' => 'varchar',
             'length' => 255,
             ));
        $this->hasColumn('url', 'varchar', 255, array(
             'type' => 'varchar',
             'length' => 255,
             ));
        $this->hasColumn('genre', 'varchar', 255, array(
             'type' => 'varchar',
             'length' => 255,
             ));
        $this->hasColumn('store_count', 'integer', null, array(
             'type' => 'integer',
             'unsigned' => true,
             ));
        $this->hasColumn('new_flag', 'boolean', null, array(
             'type' => 'boolean',
             'notnull' => true,
             'default' => 1,
             ));
        $this->hasColumn('send_document_flag', 'boolean', null, array(
             'type' => 'boolean',
             'notnull' => true,
             'default' => 0,
             ));
        $this->hasColumn('expected_flag', 'boolean', null, array(
             'type' => 'boolean',
             'notnull' => true,
             'default' => 0,
             ));
        $this->hasColumn('appointment_flag', 'boolean', null, array(
             'type' => 'boolean',
             'notnull' => true,
             'default' => 0,
             ));
        $this->hasColumn('call_again_flag', 'boolean', null, array(
             'type' => 'boolean',
             'notnull' => true,
             'default' => 0,
             ));
        $this->hasColumn('failure_receive_flag', 'boolean', null, array(
             'type' => 'boolean',
             'notnull' => true,
             'default' => 0,
             ));
        $this->hasColumn('phone_ban_flag', 'boolean', null, array(
             'type' => 'boolean',
             'notnull' => true,
             'default' => 0,
             ));
        $this->hasColumn('call_schedule', 'datetime', null, array(
             'type' => 'datetime',
             'notnull' => true,
             ));
        $this->hasColumn('memo', 'text', null, array(
             'type' => 'text',
             ));
        $this->hasColumn('send_document_fax_flag', 'boolean', null, array(
             'type' => 'boolean',
             'notnull' => true,
             'default' => 0,
             ));
    }

    public function setUp()
    {
        parent::setUp();
        $this->hasMany('CallInfo', array(
             'local' => 'id',
             'foreign' => 'store_id'));

        $this->hasMany('VisitInfo', array(
             'local' => 'id',
             'foreign' => 'store_id'));

        $this->hasMany('ProposalProduct', array(
             'local' => 'id',
             'foreign' => 'store_id'));

        $this->hasMany('DealProduct', array(
             'local' => 'id',
             'foreign' => 'store_id'));

        $this->hasMany('Contract', array(
             'local' => 'id',
             'foreign' => 'store_id'));

        $timestampable0 = new Doctrine_Template_Timestampable();
        $this->actAs($timestampable0);
    }
}