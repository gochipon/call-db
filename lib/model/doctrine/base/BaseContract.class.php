<?php

/**
 * BaseContract
 * 
 * This class has been auto-generated by the Doctrine ORM Framework
 * 
 * @property integer $id
 * @property integer $store_id
 * @property integer $product_id
 * @property date $contract_date
 * @property integer $target
 * @property integer $initial_cost
 * @property integer $period_flag
 * @property integer $payment
 * @property date $start_date
 * @property date $end_date
 * @property text $detail
 * @property Store $Store
 * @property Product $Product
 * @property Doctrine_Collection $BillingDestination
 * 
 * @method integer             getId()                 Returns the current record's "id" value
 * @method integer             getStoreId()            Returns the current record's "store_id" value
 * @method integer             getProductId()          Returns the current record's "product_id" value
 * @method date                getContractDate()       Returns the current record's "contract_date" value
 * @method integer             getTarget()             Returns the current record's "target" value
 * @method integer             getInitialCost()        Returns the current record's "initial_cost" value
 * @method integer             getPeriodFlag()         Returns the current record's "period_flag" value
 * @method integer             getPayment()            Returns the current record's "payment" value
 * @method date                getStartDate()          Returns the current record's "start_date" value
 * @method date                getEndDate()            Returns the current record's "end_date" value
 * @method text                getDetail()             Returns the current record's "detail" value
 * @method Store               getStore()              Returns the current record's "Store" value
 * @method Product             getProduct()            Returns the current record's "Product" value
 * @method Doctrine_Collection getBillingDestination() Returns the current record's "BillingDestination" collection
 * @method Contract            setId()                 Sets the current record's "id" value
 * @method Contract            setStoreId()            Sets the current record's "store_id" value
 * @method Contract            setProductId()          Sets the current record's "product_id" value
 * @method Contract            setContractDate()       Sets the current record's "contract_date" value
 * @method Contract            setTarget()             Sets the current record's "target" value
 * @method Contract            setInitialCost()        Sets the current record's "initial_cost" value
 * @method Contract            setPeriodFlag()         Sets the current record's "period_flag" value
 * @method Contract            setPayment()            Sets the current record's "payment" value
 * @method Contract            setStartDate()          Sets the current record's "start_date" value
 * @method Contract            setEndDate()            Sets the current record's "end_date" value
 * @method Contract            setDetail()             Sets the current record's "detail" value
 * @method Contract            setStore()              Sets the current record's "Store" value
 * @method Contract            setProduct()            Sets the current record's "Product" value
 * @method Contract            setBillingDestination() Sets the current record's "BillingDestination" collection
 * 
 * @package    management
 * @subpackage model
 * @author     Your name here
 * @version    SVN: $Id: Builder.php 7691 2011-02-04 15:43:29Z jwage $
 */
abstract class BaseContract extends sfDoctrineRecord
{
    public function setTableDefinition()
    {
        $this->setTableName('contract');
        $this->hasColumn('id', 'integer', null, array(
             'type' => 'integer',
             'primary' => true,
             'autoincrement' => true,
             'unsigned' => true,
             ));
        $this->hasColumn('store_id', 'integer', null, array(
             'type' => 'integer',
             'unsigned' => true,
             'default' => 0,
             'notnull' => true,
             ));
        $this->hasColumn('product_id', 'integer', null, array(
             'type' => 'integer',
             'unsigned' => true,
             'default' => 0,
             'notnull' => true,
             ));
        $this->hasColumn('contract_date', 'date', null, array(
             'type' => 'date',
             ));
        $this->hasColumn('target', 'integer', null, array(
             'type' => 'integer',
             'unsigned' => true,
             'default' => 0,
             'notnull' => true,
             ));
        $this->hasColumn('initial_cost', 'integer', null, array(
             'type' => 'integer',
             'unsigned' => true,
             ));
        $this->hasColumn('period_flag', 'integer', null, array(
             'type' => 'integer',
             'unsigned' => true,
             ));
        $this->hasColumn('payment', 'integer', null, array(
             'type' => 'integer',
             'unsigned' => true,
             ));
        $this->hasColumn('start_date', 'date', null, array(
             'type' => 'date',
             ));
        $this->hasColumn('end_date', 'date', null, array(
             'type' => 'date',
             ));
        $this->hasColumn('detail', 'text', null, array(
             'type' => 'text',
             ));
    }

    public function setUp()
    {
        parent::setUp();
        $this->hasOne('Store', array(
             'local' => 'store_id',
             'foreign' => 'id'));

        $this->hasOne('Product', array(
             'local' => 'product_id',
             'foreign' => 'id'));

        $this->hasMany('BillingDestination', array(
             'local' => 'id',
             'foreign' => 'contract_id'));

        $timestampable0 = new Doctrine_Template_Timestampable();
        $this->actAs($timestampable0);
    }
}