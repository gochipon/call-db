<?php

/**
 * CallInfoTable
 * 
 * This class has been auto-generated by the Doctrine ORM Framework
 */
class CallInfoTable extends Doctrine_Table
{
    /**
     * Returns an instance of this class.
     *
     * @return object CallInfoTable
     */
    public static function getInstance()
    {
        return Doctrine_Core::getTable('CallInfo');
    }
}