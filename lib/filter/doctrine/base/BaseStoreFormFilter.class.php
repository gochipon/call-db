<?php

/**
 * Store filter form base class.
 *
 * @package    management
 * @subpackage filter
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormFilterGeneratedTemplate.php 29570 2010-05-21 14:49:47Z Kris.Wallsmith $
 */
abstract class BaseStoreFormFilter extends BaseFormFilterDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'region'                 => new sfWidgetFormFilterInput(),
      'prefecture'             => new sfWidgetFormFilterInput(),
      'company_name'           => new sfWidgetFormFilterInput(),
      'company_name_kana'      => new sfWidgetFormFilterInput(),
      'president'              => new sfWidgetFormFilterInput(),
      'president_kana'         => new sfWidgetFormFilterInput(),
      'person_in_charge'       => new sfWidgetFormFilterInput(),
      'person_in_charge_kana'  => new sfWidgetFormFilterInput(),
      'sales_representative'   => new sfWidgetFormFilterInput(),
      'company_address'        => new sfWidgetFormFilterInput(),
      'company_tel'            => new sfWidgetFormFilterInput(),
      'company_mobile_phone'   => new sfWidgetFormFilterInput(),
      'company_fax'            => new sfWidgetFormFilterInput(),
      'store_name'             => new sfWidgetFormFilterInput(),
      'store_address'          => new sfWidgetFormFilterInput(),
      'store_tel'              => new sfWidgetFormFilterInput(),
      'store_fax'              => new sfWidgetFormFilterInput(),
      'mail'                   => new sfWidgetFormFilterInput(),
      'url'                    => new sfWidgetFormFilterInput(),
      'genre'                  => new sfWidgetFormFilterInput(),
      'store_count'            => new sfWidgetFormFilterInput(),
      'new_flag'               => new sfWidgetFormChoice(array('choices' => array('' => 'yes or no', 1 => 'yes', 0 => 'no'))),
      'send_document_flag'     => new sfWidgetFormChoice(array('choices' => array('' => 'yes or no', 1 => 'yes', 0 => 'no'))),
      'expected_flag'          => new sfWidgetFormChoice(array('choices' => array('' => 'yes or no', 1 => 'yes', 0 => 'no'))),
      'appointment_flag'       => new sfWidgetFormChoice(array('choices' => array('' => 'yes or no', 1 => 'yes', 0 => 'no'))),
      'call_again_flag'        => new sfWidgetFormChoice(array('choices' => array('' => 'yes or no', 1 => 'yes', 0 => 'no'))),
      'failure_receive_flag'   => new sfWidgetFormChoice(array('choices' => array('' => 'yes or no', 1 => 'yes', 0 => 'no'))),
      'phone_ban_flag'         => new sfWidgetFormChoice(array('choices' => array('' => 'yes or no', 1 => 'yes', 0 => 'no'))),
      'call_schedule'          => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => false)),
      'memo'                   => new sfWidgetFormFilterInput(),
      'send_document_fax_flag' => new sfWidgetFormChoice(array('choices' => array('' => 'yes or no', 1 => 'yes', 0 => 'no'))),
      'created_at'             => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => false)),
      'updated_at'             => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => false)),
    ));

    $this->setValidators(array(
      'region'                 => new sfValidatorPass(array('required' => false)),
      'prefecture'             => new sfValidatorPass(array('required' => false)),
      'company_name'           => new sfValidatorPass(array('required' => false)),
      'company_name_kana'      => new sfValidatorPass(array('required' => false)),
      'president'              => new sfValidatorPass(array('required' => false)),
      'president_kana'         => new sfValidatorPass(array('required' => false)),
      'person_in_charge'       => new sfValidatorPass(array('required' => false)),
      'person_in_charge_kana'  => new sfValidatorPass(array('required' => false)),
      'sales_representative'   => new sfValidatorPass(array('required' => false)),
      'company_address'        => new sfValidatorPass(array('required' => false)),
      'company_tel'            => new sfValidatorPass(array('required' => false)),
      'company_mobile_phone'   => new sfValidatorPass(array('required' => false)),
      'company_fax'            => new sfValidatorPass(array('required' => false)),
      'store_name'             => new sfValidatorPass(array('required' => false)),
      'store_address'          => new sfValidatorPass(array('required' => false)),
      'store_tel'              => new sfValidatorPass(array('required' => false)),
      'store_fax'              => new sfValidatorPass(array('required' => false)),
      'mail'                   => new sfValidatorPass(array('required' => false)),
      'url'                    => new sfValidatorPass(array('required' => false)),
      'genre'                  => new sfValidatorPass(array('required' => false)),
      'store_count'            => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'new_flag'               => new sfValidatorChoice(array('required' => false, 'choices' => array('', 1, 0))),
      'send_document_flag'     => new sfValidatorChoice(array('required' => false, 'choices' => array('', 1, 0))),
      'expected_flag'          => new sfValidatorChoice(array('required' => false, 'choices' => array('', 1, 0))),
      'appointment_flag'       => new sfValidatorChoice(array('required' => false, 'choices' => array('', 1, 0))),
      'call_again_flag'        => new sfValidatorChoice(array('required' => false, 'choices' => array('', 1, 0))),
      'failure_receive_flag'   => new sfValidatorChoice(array('required' => false, 'choices' => array('', 1, 0))),
      'phone_ban_flag'         => new sfValidatorChoice(array('required' => false, 'choices' => array('', 1, 0))),
      'call_schedule'          => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 00:00:00')), 'to_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 23:59:59')))),
      'memo'                   => new sfValidatorPass(array('required' => false)),
      'send_document_fax_flag' => new sfValidatorChoice(array('required' => false, 'choices' => array('', 1, 0))),
      'created_at'             => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 00:00:00')), 'to_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 23:59:59')))),
      'updated_at'             => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 00:00:00')), 'to_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 23:59:59')))),
    ));

    $this->widgetSchema->setNameFormat('store_filters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'Store';
  }

  public function getFields()
  {
    return array(
      'id'                     => 'Number',
      'region'                 => 'Text',
      'prefecture'             => 'Text',
      'company_name'           => 'Text',
      'company_name_kana'      => 'Text',
      'president'              => 'Text',
      'president_kana'         => 'Text',
      'person_in_charge'       => 'Text',
      'person_in_charge_kana'  => 'Text',
      'sales_representative'   => 'Text',
      'company_address'        => 'Text',
      'company_tel'            => 'Text',
      'company_mobile_phone'   => 'Text',
      'company_fax'            => 'Text',
      'store_name'             => 'Text',
      'store_address'          => 'Text',
      'store_tel'              => 'Text',
      'store_fax'              => 'Text',
      'mail'                   => 'Text',
      'url'                    => 'Text',
      'genre'                  => 'Text',
      'store_count'            => 'Number',
      'new_flag'               => 'Boolean',
      'send_document_flag'     => 'Boolean',
      'expected_flag'          => 'Boolean',
      'appointment_flag'       => 'Boolean',
      'call_again_flag'        => 'Boolean',
      'failure_receive_flag'   => 'Boolean',
      'phone_ban_flag'         => 'Boolean',
      'call_schedule'          => 'Date',
      'memo'                   => 'Text',
      'send_document_fax_flag' => 'Boolean',
      'created_at'             => 'Date',
      'updated_at'             => 'Date',
    );
  }
}
