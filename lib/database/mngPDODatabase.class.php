<?php

class mngPDODatabase extends sfPDODatabase
{
  /**
   * Connects to the database.
   *
   * @throws <b>sfDatabaseException</b> If a connection could not be created
   */
  public function connect()
  {
    if (!$dsn = $this->getParameter('dsn'))
    {
      // missing required dsn parameter
      throw new sfDatabaseException('Database configuration is missing the "dsn" parameter.');
    }

    try
    {
      $pdo_class  = $this->getParameter('class', 'PDO');
      $username   = $this->getParameter('username');
      $password   = $this->getParameter('password');
      $persistent = $this->getParameter('persistent');

      //INFILEを使うためにPDO::MYSQL_ATTR_LOCAL_INFILE => trueを追加
      $options = ($persistent) ? array(PDO::ATTR_PERSISTENT => true, PDO::MYSQL_ATTR_LOCAL_INFILE => true, PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION, PDO::MYSQL_ATTR_INIT_COMMAND => "SET SESSION sql_mode='TRADITIONAL'") : array(PDO::MYSQL_ATTR_LOCAL_INFILE => true, PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION, PDO::MYSQL_ATTR_INIT_COMMAND => "SET SESSION sql_mode='TRADITIONAL'");

      $this->connection = new $pdo_class($dsn, $username, $password, $options);

    }
    catch (PDOException $e)
    {
      throw new sfDatabaseException($e->getMessage());
    }

    // lets generate exceptions instead of silent failures
    if (sfConfig::get('sf_debug'))
    {
      $this->connection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    }
    else
    {
      $this->connection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_SILENT);
    }

    // compatability
    $compatability = $this->getParameter('compat');
    if ($compatability)
    {
      $this->connection->setAttribute(PDO::ATTR_CASE, PDO::CASE_NATURAL);
    }

    // nulls
    $nulls = $this->getParameter('nulls');
    if ($nulls)
    {
      $this->connection->setAttribute(PDO::ATTR_ORACLE_NULLS, PDO::NULL_EMPTY_STRING);
    }

    // auto commit
    $autocommit = $this->getParameter('autocommit');
    if ($autocommit)
    {
      $this->connection->setAttribute(PDO::ATTR_AUTOCOMMIT, true);
    }

    $this->resource = $this->connection;

  }
}
