<?php

/**
 * symfony関連のショートハンド用クラス
 *
 * @author     yuki fukui <y_fukui@gochipon.co.jp>
 */
class MngSf
{


  /**
   *
   * @return type
   */
  public static function getContext()
  {
    return sfContext::getInstance();
  }

  /**
   *
   * @return type
   */
  public static function getUser()
  {
    return self::getContext()->getUser();
  }


}
