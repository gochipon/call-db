<?php

/**
 *
 * @author     yuki fukui <y_fukui@gochipon.co.jp>
 */
class Mng
{


  public static function getAccountId()
  {
    static $id = null;
    if ($id !== null) {
      return $id;
    }

    //セッションから取得
    if ($id = MngSf::getUser()->getAttribute('account_id')) {
      return $id;
    }

    return $id;
  }

  public static function getAccount()
  {
    return AccountTable::getInstance()->findOneById(Mng::getAccountId());
  }



}
