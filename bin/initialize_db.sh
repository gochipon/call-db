#!/bin/sh
source $(dirname $0)/../data/shell/common.sh

function drop_tables()
{
  echo 'drop table if exists doctrine__record__abstract' | $MYSQL
}

function initialize_db()
{
  $SYMFONY doctrine:build --all-classes
  $SYMFONY doctrine:build --sql
  $SYMFONY doctrine:drop-db --no-confirmation
  $SYMFONY doctrine:build-db
  $SYMFONY doctrine:data-load
}

# main
confirm 'Do you really want to reset the database?'
initialize_db
drop_tables
