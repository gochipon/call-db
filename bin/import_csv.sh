#!/bin/bash
source $(dirname $0)/../data/shell/common.sh
CSV_DIR=$DATA_DIR/csv

function import_csv_files() {
  find $CSV_DIR/init-import/*.csv | while read csv_file; do
    tablename=$(basename $csv_file | sed 's/[0-9]*_\(.*\)\.csv/\1/')
    echo ">> csv       $csv_file"
    echo "load data local infile \"$csv_file\"" \
      "into table $tablename" \
      "fields terminated by ','" \
      "ENCLOSED BY '\"'" \
      "LINES TERMINATED BY '\\n'" \
      ";" | $MYSQL --local-infile=1
  done
  echo '>> csv       loaded successfully'
}

# main
import_csv_files
