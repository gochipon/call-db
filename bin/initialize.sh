#!/bin/sh
source $(dirname $0)/../data/shell/common.sh

function cache_clear()
{
  if is_symfony_installed; then
    $SYMFONY cache:clear
  fi
}

function create_js_files()
{
  $BIN_DIR/create_js_files.sh
}

function fix_permissions()
{
  chmod a+w $CACHE_DIR
  chmod a+w $LOG_DIR
}

function make_directories()
{
  xmkdir $CACHE_DIR
  xmkdir $LOG_DIR
  xmkdir $TMP_DIR
}

function initialize_sample_files()
{
  find $APP_DIR $CONFIG_DIR -name '*.sample' | while read sample_file; do
    file=${sample_file%.sample}
    if [ ! -f $file ]; then
      cp $sample_file $file
    fi
  done
}

function publish_assets()
{
  if is_symfony_installed; then
    $SYMFONY plugin:publish-assets
  fi
}

# main
initialize_sample_files
make_directories
fix_permissions
create_js_files
cache_clear
publish_assets
$BIN_DIR/update_mo.sh
