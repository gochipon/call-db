<?php

class myUser extends sfBasicSecurityUser
{
  /**
   * ログイン処理
   */
  public function login($account_record)
  {
    $this->setAuthenticated(true);
    $this->setAttribute('account_id', $account_record['id']);
    $this->setAttribute('authority_group_id', $account_record['authority_group_id']);
  }

  /**
   * ログアウト処理
   */
  public function logout()
  {
    $this->setAuthenticated(false);
    $this->getAttributeHolder()->remove('account_id');
    $this->getAttributeHolder()->remove('authority_group_id');
    $this->clearCredentials();
  }

  /**
   * 権限を取得
   */
  public function getAuthorityId()
  {
    //セッションが取得できるかチェック
    return $this->getAttribute('authority_group_id');
  }

  /**
   * adminユーザーかどうか確認する
   */
  public function checkAdmin()
  {
    $authority_id = $this->getAuthorityId();

    if ($authority_id == 1) {
      return true;
    } else {
      return false;
    }
  }


  /**
   * 認証済みか確認
   */
  public function isAuthenticated()
  {
    //セッションが取得できるかチェック
    $id = $this->getAttribute('account_id');
    if ($id) {
      return true;
    } else {
      return false;
    }
  }

}
