<? if ($sf_user->hasFlash('error')) : ?></li>
</ul>

<div class="error_list"><?= $sf_user->getFlash('error') ?></div>

<? endif ?>

<form action="<?= url_for('csv/upload') ?>" method="post" enctype="multipart/form-data" >
    <?= $form ?>
    <input type="submit" value="CSVアップロード" onclick='return confirm(record_count + "件の情報をインポートします。よろしいですか？");'>
</form>

<div id="result"></div>

<script>
$('#file_csv').attr('onchange', 'checkRecordCount()');

var record_count = 0;
var checkRecordCount = function() {
  var file = $('#file_csv')[0].files[0];
  var fileContents = "";
  var reader = new FileReader();
  reader.readAsText(file, "utf-8");
  reader.onload = function(e){
    var txt = e.target.result;
    var n = txt.match(/[\n\r]/g);
    if (n) {
      record_count = n.length;
    } else {
      record_count = 0;
    }
  }
}
</script>
