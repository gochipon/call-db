<?php

/**
 * csv actions.
 *
 * @package    management
 * @subpackage csv
 * @author     Your name here
 * @version    SVN: $Id: actions.class.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class csvActions extends sfActions
{
 /**
  * Executes index action
  */
  public function executeIndex(sfWebRequest $request)
  {
    $this->form = new CsvUploadForm();
  }

 /**
  * upload処理
  */
  public function executeUpload(sfWebRequest $request)
  {
    ini_set('memory_limit', '16384M');
    ini_set("max_execution_time", 300000);

    if ($request->isMethod('post')) {
      // フォームインスタンス生成後、bind
      $form = new CsvUploadForm();
      $form->bind($request->getParameter($form->getName()), $request->getFiles('file'));

      $file = $form->getValue('csv');
      $file_path = $file->getOriginalName();

      //fileのチェック
      setlocale(LC_ALL, 'ja_JP.UTF-8');
      $data = file_get_contents($file->getTempName());
      $temp = tmpfile();
      $csv_array = array();
      fwrite($temp, $data);
      rewind($temp);

      while (($data = fgetcsv($temp, 0, ",")) !== FALSE) {
        $csv_array[] = $data;
      }
      fclose($temp);

      $all_record_count = count($csv_array);

      foreach ($csv_array as $atom) {
        //FIXME: カラム追加時にカラム数の判定の値も増やす
        if (count($atom) != 34) {
          die('csvが不正です。確認してください。');
        }
      }

      // ファイル移動
      @move_uploaded_file($file->getTempName(), $file_path);

      $conn = Doctrine_Manager::connection();
      $conn->beginTransaction();
      try {
        $complete_csv_array = array();
        foreach ($csv_array as $key => $atom) {
          //同じ電話番号のものがあった場合は弾く
          $exist_record = Doctrine_Query::create()
            ->from('Store store')
            ->where('store.company_tel = ?', $atom[8])
            ->orWhere('store.company_tel = ?', '※' . $atom[8])
            ->count();
          if ($exist_record > 0) {
            continue;
          }

          $complete_csv_array[] = $atom;

          $store_record = new Store();
          $store_record->setRegion($atom[1]);
          $store_record->setPrefecture($atom[2]);
          $store_record->setCompanyName($atom[3]);
          $store_record->setPresident($atom[4]);
          $store_record->setPersonInCharge($atom[5]);
          $store_record->setSalesRepresentative($atom[6]);
          $store_record->setCompanyAddress($atom[7]);
          $store_record->setCompanyTel($atom[8]);
          $store_record->setCompanyFax($atom[9]);
          $store_record->setStoreName($atom[10]);
          $store_record->setStoreAddress($atom[11]);
          $store_record->setStoreTel($atom[12]);
          $store_record->setStoreFax($atom[13]);
          $store_record->setMail($atom[14]);
          $store_record->setUrl($atom[15]);
          $store_record->setGenre($atom[16]);
          $store_record->setStoreCount($atom[17] === '' ? 0 : $atom[17]);
          if ($atom[18] !== '') {
            $store_record->setNewFlag($atom[18]);
          }
          if ($atom[19] !== '') {
            $store_record->setCallAgainFlag($atom[19]);
          }
          if ($atom[20] !== '') {
            $store_record->setFailureReceiveFlag($atom[20]);
          }
          if ($atom[21] !== '') {
            $store_record->setPhoneBanFlag($atom[21]);
          }
          $store_record->setCallSchedule($atom[22] === '' ? '0000-00-00 00:00:00' : $atom[22]);
          $store_record->setMemo($atom[23]);
          if ($atom[26] !== '') {
            $store_record->setSendDocumentFlag($atom[26]);
          }
          $store_record->setCompanyNameKana($atom[27]);
          $store_record->setPresidentKana($atom[28]);
          $store_record->setPersonInChargeKana($atom[29]);
          if ($atom[30] !== '') {
            $store_record->setExpectedFlag($atom[30]);
          }
          if ($atom[31] !== '') {
            $store_record->setAppointmentFlag($atom[31]);
          }
          $store_record->setCompanyMobilePhone($atom[32]);
          if ($atom[33] !== '') {
            $store_record->setSendDocumentFaxFlag($atom[33]);
          }
          $store_record->save($conn);
        }

        $conn->commit();
      } catch (Exception $e) {
        $conn->rollBack();
        throw $e;
      }
      //ファイル削除
      @unlink($file_path);

      //インポート完了したcsvのデータをセッションに保存
      $this->getUser()->setAttribute('complete_csv_array', $complete_csv_array);
      
      $this->redirect('csv/uploadComplete?all_record_count=' . $all_record_count . '&success_count=' . count($complete_csv_array));
    }

    //ファイル削除
    @unlink($file_path);

    $this->getUser()->setFlash('error', 'アップロードするファイルは、"user.csv"としてアップロードしてください');
    $this->redirect('csv/index');
  }

  /**
   * インポート完了データのcsvダウンロード
   */
  public function executeDownloadCompleteCsv(sfWebRequest $request)
  {
    //インポートしたデータをファイルで出力する処理
    $complete_csv_array = $this->getUser()->getAttribute('complete_csv_array', array());
    $complete_csv = '';
    foreach ($complete_csv_array as $key => $atom){
      for ($i = 0; $i < count($complete_csv_array[$key]); $i++) {
        $complete_csv .= '"';
        $complete_csv .= $atom[$i];
        $complete_csv .= '"';
        if ($i < count($complete_csv_array[$key]) - 1) {
          $complete_csv .= ",";
        }
      }
      $complete_csv .= "\n";
    }
    $fileName = "import_" . date("YmdHis") . ".csv";
    header('Content-Type: application/octet-stream');
    header('Content-Disposition: attachment; filename=' . $fileName);
    echo $complete_csv;
    die();
  }

  /**
   * ※未使用
   * upload処理
   */
  public function executePDOUpload(sfWebRequest $request)
  {
    if ($request->isMethod('post')) {
      // フォームインスタンス生成後、bind
      $form = new CsvUploadForm();
      $form->bind($request->getParameter($form->getName()), $request->getFiles('file'));

      $file = $form->getValue('csv');
      $file_path = $file->getOriginalName();

      //fileのチェック
      setlocale(LC_ALL, 'ja_JP.UTF-8');
      $data = file_get_contents($file->getTempName());
      $temp = tmpfile();
      $csv_array = array();
      fwrite($temp, $data);
      rewind($temp);

      while (($data = fgetcsv($temp, 0, ",")) !== FALSE) {
        $csv_array[] = $data;
      }
      fclose($temp);

      $all_record_count = count($csv_array);

      foreach ($csv_array as $atom) {
        //FIXME: カラム追加時にカラム数の判定の値も増やす
        if (count($atom) != 34) {
          die('csvが不正です。確認してください。');
        }
      }

      // ファイル移動
      @move_uploaded_file($file->getTempName(), $file_path);

      // csvファイルからレコード追加
      $sql = sprintf(
        "load data local infile \"%s\" into table store fields terminated by ',' ENCLOSED BY '\"' LINES TERMINATED BY '\\n';",
        $file_path
      );

      $pdo = $this->getContext()->getDatabaseConnection('pdocon');
      $pdo->beginTransaction();
      try {
        $success_count = $pdo->exec($sql);
        $pdo->commit();
      } catch (PDOException $e) {
        $pdo->rollBack();
        @unlink($file_path);
        die($e->getMessage());
      }

      //ファイル削除
      @unlink($file_path);
      $pdo = null;

      $this->redirect('csv/uploadComplete?all_record_count=' . $all_record_count . '&success_count=' . $success_count);
    }

    $this->getUser()->setFlash('error', 'アップロードするファイルは、"user.csv"としてアップロードしてください');
    $this->redirect('csv/index');
  }

 /**
  * アップロード完了処理
  */
  public function executeUploadComplete(sfWebRequest $request)
  {
    $this->all_record_count = (int) $request->getParameter('all_record_count');
    $this->success_count    = (int) $request->getParameter('success_count');
  }

}
