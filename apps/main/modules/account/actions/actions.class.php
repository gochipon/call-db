<?php

/**
 * account actions.
 *
 * @package    management
 * @subpackage account
 * @author     Your name here
 * @version    SVN: $Id: actions.class.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class accountActions extends sfActions
{
 /**
  * Executes index action
  *
  * @param sfRequest $request A request object
  */
  public function executeIndex()
  {
    $this->is_admin = $this->getUser()->checkAdmin(); 
  }

  /**
   * ユーザー追加ページ
   */
  public function executeAddUser(sfWebRequest $request)
  {
    $this->checkAdmin();

    if ($request->isMethod('post')) {
      $post_data = $request->getPostParameters();

      foreach ($post_data as $atom) {
        if ($atom === '') {
          $this->error_message = '入力抜けがあります。';
          return;
        }
      }

      if ($post_data['user_pass'] !== $post_data['user_pass_confirm']) {
        $this->error_message = 'パスワードが一致しません';
        return;
      }

      $count = Doctrine_Query::create()
        ->from('Account account')
        ->where('account.name = ?', $post_data['user_id'])
        ->count();
      
      if ($count > 0) {
        $this->error_message = '既にこのユーザーIDは使用されています';
        return; 
      }

      Top::saveNewAccount($post_data); 
      $this->redirect('account/addUserConfirm');

    }

    $this->error_message = '';

  }

  /**
   * ユーザー追加確認ページ
   */
  public function executeAddUserConfirm(sfWebRequest $request)
  {

  }

  /**
   * パスワード変更ページ
   */
  public function executeChangePassword(sfWebRequest $request)
  {
    $this->account_record = Mng::getAccount();

    if ($request->isMethod('post')) {
      $post_data = $request->getPostParameters();

      foreach ($post_data as $atom) {
        if ($atom === '') {
          $this->error_message = '入力抜けがあります。';
          return;
        }
      }

      if (crypt($post_data['current_pass'], $this->account_record['password']) !== $this->account_record['password']) {
        $this->error_message = '現在のパスワードが違います';
        return;
      }

      if ($post_data['new_pass'] !== $post_data['new_pass_confirm']) {
        $this->error_message = '新しいパスワードが一致しません';
        return;
      }

      Top::changePassword($post_data); 
      $this->redirect('account/changePasswordConfirm');

    }

    $this->error_message = '';
  }

  /**
   * パスワード変更確認ページ
   */
  public function executeChangePasswordConfirm(sfWebRequest $request)
  {

  }

  /**
   * ユーザー一覧ページ
   */
  public function executeUserList(sfWebRequest $request)
  {
    $this->checkAdmin();

    $this->account_records = Doctrine_Query::create()
      ->from('Account')
      ->fetchArray();
  }

  /**
   * ユーザー権限変更処理
   */
  public function executeChangeUserSubmit(sfWebRequest $request)
  {
    $this->checkAdmin();

    $post_data = $request->getPostParameters();
    $account = AccountTable::getInstance()->findOneById($post_data['account_id']);
    $account->setAuthorityGroupId($post_data['credential']);
    $account->save();

    $this->redirect('account/userList');
  }

  /**
   * 表示名変更ページ
   */
  public function executeChangeNickname(sfWebRequest $request)
  {
    $account = Mng::getAccount();
    $this->nickname = $account->getNickname();
  }

  /**
   * 表示名変更処理
   */
  public function executeChangeNicknameSubmit(sfWebRequest $request)
  {
    $post_data = $request->getPostParameters();
    $account = Mng::getAccount();
    $account->setNickname($post_data['nickname']);
    $account->save();

    $this->redirect('account/changeNickname');
  }

  /**
   * コール件数確認ページ
   */
  public function executeCheckCall(sfWebRequest $request)
  {
    $this->checkAdmin();

    $post_data = $request->getPostParameters();
    if (isset($post_data['start_year']) && isset($post_data['start_month']) && isset($post_data['start_day']) && isset($post_data['start_hour']) && isset($post_data['start_min'])) {
      $start_date = $post_data['start_year'] . '-' . $post_data['start_month'] . '-' . $post_data['start_day'] . ' ' . $post_data['start_hour'] . ':' . $post_data['start_min'] . ':' . '00';
    } else {
      $start_date = date("Y-m-d") . ' 00:00:00';
    }
    if (isset($post_data['end_year']) && isset($post_data['end_month']) && isset($post_data['end_day']) && isset($post_data['end_hour']) && isset($post_data['end_min'])) {
      $end_date = $post_data['end_year'] . '-' . $post_data['end_month'] . '-' . $post_data['end_day'] . ' ' . $post_data['end_hour'] . ':' . $post_data['end_min'] . ':' . '00';
    } else {
      $end_date = date("Y-m-d") . ' 23:59:59';
    }

    $account_records = Doctrine_Query::create()
      ->from('Account a')
      ->andWhereNotIn('a.id', array(1, 2))
      ->andWhere('a.nickname > ?', 0)
      ->fetchArray();

    foreach ($account_records as $key => $account_record) {
      $call_count = Doctrine_Query::create()
        ->from('CallInfo ci')
        ->andWhere('ci.caller = ?', $account_record['nickname'])
        ->andWhere('ci.call_date >= ?', $start_date)
        ->andWhere('ci.call_date <= ?', $end_date)
        ->count();

      $account_records[$key]['call_count'] = $call_count;
    }

    $this->account_records = $account_records;
    $this->start_date = $start_date;
    $this->end_date   = $end_date;
  }

  /**
   * ユーザー削除処理
   */
  public function executeDeleteUserSubmit(sfWebRequest $request)
  {
    $this->checkAdmin();

    $post_data = $request->getPostParameters();
    $account = AccountTable::getInstance()->findOneById($post_data['account_id']);
    $account->delete();

    $this->redirect('account/userList');
  }

  /**
   * adminユーザーかどうか確認する
   */
  private function checkAdmin()
  {
    $authority_id = $this->getUser()->getAuthorityId();
    if ($authority_id !== '1') {
      $this->redirect('client/search');
    }
  }
}
