<div id="login">
  <h1>ユーザー一覧</h1>
  <? foreach ($account_records as $atom) { ?>
    <form method="post" class="once_submit" style="display: inline" action="<?= url_for('account/changeUserSubmit') ?>">
      <?= $atom['name'] ?>&nbsp;
      <input type="hidden" name="account_id" value="<?= $atom['id'] ?>">
      <select name="credential">
        <option value="1" <? if ($atom['authority_group_id'] == 1) echo 'selected'; ?>>admin</option>
        <option value="2" <? if ($atom['authority_group_id'] == 2) echo 'selected'; ?>>full_user</option>
        <option value="3" <? if ($atom['authority_group_id'] == 3) echo 'selected'; ?>>limited_user</option>
      </select>
      <br>
      <input type="submit" name="submit" value="変更" />
    </form>
    <form method="post" class="once_submit" style="display: inline" action="<?= url_for('account/deleteUserSubmit') ?>">
      <input type="hidden" name="account_id" value="<?= $atom['id'] ?>">
      <input type="submit" onclick="return confirm('このユーザーを削除してよろしいですか？')" name="submit" value="削除" />
    </form>
    <br><br>    
  <? } ?>
    <br>
  <?= link_to('戻る', 'account/index') ?>
</div>
