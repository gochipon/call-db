<?= $start_date; ?> 〜 <?= $end_date; ?>
<table border="2" width="500">
  <tbody>
    <tr>
      <td width="300">名前</td>
      <td width="200">コール件数</td>
    </tr>
    <? foreach ($account_records as $account_record) { ?>
    <tr>
      <td width="300"><?= $account_record['nickname'] ?></td>
      <td width="200"><?= $account_record['call_count'] ?>件</td>
    </tr>
    <? } ?>
  </tbody>
</table>

<form class="once_submit" method="POST" action="<?= url_for('account/checkCall') ?>">
  <? list($start_year, $start_month, $start_day, $start_hour, $start_min, $start_sec) = preg_split("/[-: ]/", $start_date); ?>
  <? list($end_year, $end_month, $end_day, $end_hour, $end_min, $end_sec) = preg_split("/[-: ]/", $end_date); ?>
  <? include_partial('account/datetimeExistForm', array('name' => 'start', 'year' => $start_year, 'month' => $start_month, 'day' => $start_day, 'hour' => $start_hour, 'min' => $start_min)) ?>
  <br>〜<br>
  <? include_partial('account/datetimeExistForm', array('name' => 'end', 'year' => $end_year, 'month' => $end_month, 'day' => $end_day, 'hour' => $end_hour, 'min' => $end_min)) ?>
  <button type="submit" class="btn btn-default">検索</button>
</form>
