<div id="login">
  <h1>ユーザー追加</h1>
  <p class="font_red"><?= $error_message ?></p>
  <form method="post" class="once_submit" action="<?= url_for('account/addUser') ?>">
    <dl>
      <dt>ログインID:</dt>
      <dd><input type="text" name="user_id" /></dd>
      <dt>パスワード:</dt>
      <dd><input type="password" name="user_pass" /></dd>
      <dt>パスワードの再入力:</dt>
      <dd><input type="password" name="user_pass_confirm" /></dd>
      <dt>権限:</dt>
      <dd>
        <select name="credential">
          <option value="1">admin</option>
          <option value="2">full_user</option>
          <option value="3">limited_user</option>
        </select>
      </dd>
    </dl>
    <p><input type="submit" name="submit" value="ユーザーを追加" /></p>
  </form>
  <br><br>
  <?= link_to('戻る', 'account/index') ?>
</div>
<script>
$(function(){
  $('.once_submit').disableOnSubmit();
});
</script>
