<div id="login">
  <h1>アカウントページ</h1>
  <?= link_to('パスワード変更', 'account/changePassword') ?><br>
  <?= link_to('表示名変更', 'account/changeNickname') ?><br>
  <? if ($is_admin) { ?>
    <?= link_to('ユーザー追加', 'account/addUser') ?><br>
    <?= link_to('ユーザー一覧', 'account/userList') ?><br>
    <?= link_to('コール件数確認', 'account/checkCall') ?><br>
  <? } ?>
  <br>
  <br>
  <?= link_to('戻る', 'client/show') ?>
</div>
