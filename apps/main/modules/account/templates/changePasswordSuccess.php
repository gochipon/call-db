<div id="login">
  <h1>パスワード変更</h1>
  <p class="font_red"><?= $error_message ?></p>
  <form method="post" class="once_submit" action="<?= url_for('account/changePassword') ?>">
    <dl>
      <dt>ログインID:</dt>
      <dd><?= $account_record['name'] ?></dd>
      <dt>現在のパスワード:</dt>
      <dd><input type="password" name="current_pass" /></dd>
      <dt>新しいパスワード:</dt>
      <dd><input type="password" name="new_pass" /></dd>
      <dt>新しいパスワードの再入力:</dt>
      <dd><input type="password" name="new_pass_confirm" /></dd>
    </dl>
    <p><input type="submit" name="submit" value="パスワード変更" /></p>
    <br>
    <br>
    <?= link_to('戻る', 'account/index') ?>
  </form>
</div>
<script>
$(function(){
  $('.once_submit').disableOnSubmit();
});
</script>
