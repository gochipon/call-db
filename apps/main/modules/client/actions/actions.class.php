<?php

/**
 * client actions.
 *
 * @package    management
 * @subpackage client
 * @author     Your name here
 * @version    SVN: $Id: actions.class.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class clientActions extends sfActions
{

  /**
   * 検索ページ
   */
  public function executeSearch()
  {

  }

  public function executeOut()
  {
    $this->q = Doctrine_Query::create()
      ->from('Store store')
      ->where('store.sales_representative LIKE ?', '%' . '伊藤' . '%')
      ->leftJoin('store.CallInfo call_info')
      ->fetchArray();

    $file = "out.txt";
    
    $fp = fopen("out.txt", "a");
    foreach ($this->q as $str) {
      $string = $str['company_name'];
      if (isset($str['CallInfo'])) {
        foreach ($str['CallInfo'] as $call) {
          $string .= ',' . $call['detail'];
        }
      }
      $string .= "\n";
      fwrite($fp, $string);
    }
    fclose($fp);

  }

  public function executeList()
  {

  }

  public function executeNew()
  {

  }

  /**
   * 詳細表示ページ
   */
  public function executeShow(sfWebRequest $request)
  {
    $this->account_record = Mng::getAccount();
    $this->edit_flag = $this->account_record['authority_group_id'] == 3 ? 'disabled' : '';
    $this->check_edit = $this->account_record['authority_group_id'] == 3 ? 'return false' : '';

    $search = $this->getRequest()->getCookie('search');

    //list_numの読み込みと保存
    $this->list_num = $this->getRequestParameter('list_num');
    if(is_null($this->list_num)) {
      if(isset($search['list_num'])) {
        $this->list_num = $search['list_num'];
      } else {
        $this->list_num = 0;
      }
    }
    $this->list_num = (int) $this->list_num;
    $this->getResponse()->setCookie("search[list_num]", '', time() - 3600, '/');
    $this->getResponse()->setCookie("search[list_num]", $this->list_num, time() + 60 * 60 * 24 * 30);

    //pager_idの読み込みと保存
    $this->pager_id = $this->getRequestParameter('pager_id');
    if(is_null($this->pager_id)) {
      if(isset($search['pager_id'])) {
        $this->pager_id = $search['pager_id'];
      } else {
        $this->pager_id = 1;
      }
    }
    $this->pager_id = (int) $this->pager_id;
    $this->getResponse()->setCookie("search[pager_id]", '', time() - 3600, '/');
    $this->getResponse()->setCookie("search[pager_id]", $this->pager_id, time() + 60 * 60 * 24 * 30);
    

    $q = Doctrine_Query::create()
      ->from('Store store');

    if(isset($search['new_flag']) && !isset($search['exist_flag'])) {
      $q->andWhere('store.new_flag = ?', 1);
    } else if(!isset($search['new_flag']) && isset($search['exist_flag'])) {
      $q->andWhere('store.new_flag = ?', 0);
    }
    if(isset($search['send_document_true_flag']) && !isset($search['send_document_false_flag'])) {
      $q->andWhere('store.send_document_flag = ?', 1);
    } else if(!isset($search['send_document_true_flag']) && isset($search['send_document_false_flag'])) {
      $q->andWhere('store.send_document_flag = ?', 0);
    }
    if(isset($search['send_document_fax_true_flag']) && !isset($search['send_document_fax_false_flag'])) {
      $q->andWhere('store.send_document_fax_flag = ?', 1);
    } else if(!isset($search['send_document_fax_true_flag']) && isset($search['send_document_fax_false_flag'])) {
      $q->andWhere('store.send_document_fax_flag = ?', 0);
    }
    if(isset($search['expected_true_flag']) && !isset($search['expected_false_flag'])) {
      $q->andWhere('store.expected_flag = ?', 1);
    } else if(!isset($search['expected_true_flag']) && isset($search['expected_false_flag'])) {
      $q->andWhere('store.expected_flag = ?', 0);
    }
    if(isset($search['appointment_flag'])) {
      $q->andWhere('store.appointment_flag = ?', 1);
    }
    if(isset($search['call_again_flag'])) {
      $q->andWhere('store.call_again_flag = ?', 1);
    }
    if(isset($search['call_again_sort_flag'])) {
      $q->andWhere('store.call_schedule != ?', '0000-00-00')
        ->orderBy('store.call_schedule ASC');
    }
    if(isset($search['failure_receive_true_flag']) && !isset($search['failure_receive_false_flag'])) {
      $q->andWhere('store.failure_receive_flag = ?', 1);
    } else if(!isset($search['failure_receive_true_flag']) && isset($search['failure_receive_false_flag'])) {
      $q->andWhere('store.failure_receive_flag = ?', 0);
    }
    if(isset($search['phone_ban_flag'])) {
      $q->andWhere('store.phone_ban_flag = ?', 1);
    }
    if(isset($search['call_schedule'])) {
      $q->andWhere('store.call_schedule >= ?', $search['call_schedule'] . " 00:00:00")
        ->andWhere('store.call_schedule <= ?', $search['call_schedule'] . " 23:59:59");
    }
    if(isset($search['company_name_kana'])) {
      $q->andWhere('store.company_name_kana LIKE ?', '%' . $search['company_name_kana'] . '%');
    }
    if(isset($search['company_name'])) {
      $q->andWhere('store.company_name LIKE ?', '%' . $search['company_name'] . '%');
    }
    if(isset($search['president_kana'])) {
      $q->andWhere('store.president_kana LIKE ?', '%' . $search['president_kana'] . '%');
    }
    if(isset($search['president'])) {
      $q->andWhere('store.president LIKE ?', '%' . $search['president'] . '%');
    }
    if(isset($search['person_in_charge_kana'])) {
      $q->andWhere('store.person_in_charge_kana LIKE ?', '%' . $search['person_in_charge_kana'] . '%');
    }
    if(isset($search['person_in_charge'])) {
      $q->andWhere('store.person_in_charge LIKE ?', '%' . $search['person_in_charge'] . '%');
    }
    if(isset($search['company_address'])) {
      $q->andWhere('store.company_address LIKE ?', '%' . $search['company_address'] . '%');
    }
    if(isset($search['company_fax'])) {
      $q->andWhere('store.company_fax LIKE ?', '%' . $search['company_fax'] . '%');
    }
    if(isset($search['store_name'])) {
      $q->andWhere('store.store_name LIKE ?', '%' . $search['store_name'] . '%');
    }
    if(isset($search['store_address'])) {
      $q->andWhere('store.store_address LIKE ?', '%' . $search['store_address'] . '%');
    }
    if(isset($search['store_fax'])) {
      $q->andWhere('store.store_fax LIKE ?', '%' . $search['store_fax'] . '%');
    }
    if(isset($search['region'])) {
      $q->andWhere('store.region LIKE ?', '%' . $search['region'] . '%');
    }
    if(isset($search['prefecture'])) {
      $q->andWhere('store.prefecture LIKE ?', '%' . $search['prefecture'] . '%');
    }
    if(isset($search['mail'])) {
      $q->andWhere('store.mail LIKE ?', '%' . $search['mail'] . '%');
    }
    if(isset($search['url'])) {
      $q->andWhere('store.url LIKE ?', '%' . $search['url'] . '%');
    }
    if(isset($search['store_count'])) {
      $q->andWhere('store.store_count = ?', $search['store_count']);
    }
    if(isset($search['company_tel'])) {
      $q->andWhere('store.company_tel LIKE ?', '%' . $search['company_tel'] . '%');
    }
    if(isset($search['company_mobile_phone'])) {
      $q->andWhere('store.company_mobile_phone LIKE ?', '%' . $search['company_mobile_phone'] . '%');
    }
    if(isset($search['store_tel'])) {
      $q->andWhere('store.store_tel LIKE ?', '%' . $search['store_tel'] . '%');
    }
    if(isset($search['sales_representative'])) {
      $q->andWhere('store.sales_representative LIKE ?', '%' . $search['sales_representative'] . '%');
    }
    if(isset($search['genre'])) {
      $q->andWhere('store.genre LIKE ?', '%' . $search['genre'] . '%');
    }
    if(isset($search['memo'])) {
      $q->andWhere('store.memo LIKE ?', '%' . $search['memo'] . '%');
    }

    $q->leftJoin('store.CallInfo call_info')
      ->leftJoin('store.VisitInfo visit_info')
      ->leftJoin('store.ProposalProduct pp')
      ->leftJoin('pp.Product ppp')
      ->leftJoin('store.DealProduct dp')
      ->leftJoin('dp.Product dpp')
      ->leftJoin('store.Contract contract')
      ->leftJoin('contract.Product cp')
      ->leftJoin('contract.BillingDestination bd');

    if(isset($search['pp_gochipon'])) {
      $q->andWhere('ppp.name = ?', 'ごちぽん');
    }
    if(isset($search['pp_application'])) {
      $q->andWhere('ppp.name = ?', 'アプリ');
    }
    if(isset($search['pp_possession_flag'])) {
      $q->andWhere('pp.possession_flag = ?', 1);
    }
    if(isset($search['pp_addnet'])) {
      $q->andWhere('ppp.name = ?', 'アドネットワーク広告');
    }
    if(isset($search['pp_region'])) {
      $q->andWhere('pp.region LIKE ?', '%' . $search['pp_region'] . '%');
    }
    if(isset($search['pp_prefecture'])) {
      $q->andWhere('pp.prefecture LIKE ?', '%' . $search['pp_prefecture'] . '%');
    }
    if(isset($search['call_caller'])) {
      $q->andWhere('call_info.caller LIKE ?', '%' . $search['call_caller'] . '%');
    }
    if(isset($search['call_detail'])) {
      $q->andWhere('call_info.detail LIKE ?', '%' . $search['call_detail'] . '%');
    }

    $count = $q->count();

    $this->per_page = 10;
    $this->next_flag = $count > $this->per_page * $this->pager_id ? true : false;
    $this->max_pager_id = (int) ($count / $this->per_page) + 1;

    $this->store_records = $q->limit($this->per_page)->offset($this->per_page * ($this->pager_id - 1))->fetchArray(); 

    if(!isset($this->store_records[0])) {
      $this->redirect('client/search');
    }

    $show_record_store_id = $this->store_records[$this->list_num]['id'];

    $show_records = Doctrine_Query::create()
      ->from('Store store')
      ->andWhere('store.id = ?', $show_record_store_id)
      ->leftJoin('store.CallInfo call_info')
      ->leftJoin('store.VisitInfo visit_info')
      ->leftJoin('store.ProposalProduct pp')
      ->leftJoin('pp.Product ppp')
      ->leftJoin('store.DealProduct dp')
      ->leftJoin('dp.Product dpp')
      ->leftJoin('store.Contract contract')
      ->leftJoin('contract.Product cp')
      ->leftJoin('contract.BillingDestination bd')
      ->fetchArray();

    $this->show_record = $show_records[0];

  }

  /**
   * 店舗情報変更処理
   */
  public function executeChangeSubmit()
  {
    $post_data = $this->getRequest()->getPostParameters();
    Client::saveStoreChange($post_data);
    
    $this->redirect('client/show');
  }

  /**
   * コール内容変更処理
   */
  public function executeCallChangeSubmit()
  {
    $call_info = $this->getRequest()->getPostParameters();
    Client::saveChangeCall($call_info);

    $this->redirect('client/show');
  }

  /**
   * コール内容追加処理
   */
  public function executeCallAddSubmit()
  {
    $call_info = $this->getRequest()->getPostParameters();
    Client::saveAddCall($call_info);

    $this->redirect('client/show');
  }

  /**
   * 訪問内容変更処理
   */
  public function executeVisitChangeSubmit()
  {
    $call_info = $this->getRequest()->getPostParameters();
    Client::saveChangeVisit($call_info);

    $this->redirect('client/show');
  }

  /**
   * 訪問内容追加処理
   */
  public function executeVisitAddSubmit()
  {
    $call_info = $this->getRequest()->getPostParameters();
    Client::saveAddVisit($call_info);

    $this->redirect('client/show');
  }

  /**
   * 提案商材内容変更処理
   */
  public function executePpChangeSubmit()
  {
    $pp = $this->getRequest()->getPostParameters();
    Client::saveChangePp($pp);

    $this->redirect('client/show');
  }

  /**
   * 検索決定処理
   */
  public function executeSearchSubmit()
  {
    $this->account_record = Mng::getAccount();
    //cookieの削除処理
    $search = $this->getRequest()->getCookie('search');
    if(isset($search)) {
      foreach($search as $key => $atom) {
        $this->getResponse()->setCookie("search[$key]", '', time() - 3600, '/');
      }
    }

    $post_data = $this->getRequest()->getPostParameters();
    //日付の形式を変更
    if($post_data['call_year'] !== '' && $post_data['call_month'] !== '' && $post_data['call_day'] !== '') {
      $post_data['call_schedule'] = $post_data['call_year'] . '-' . $post_data['call_month'] . '-' . $post_data['call_day'];
    }
    //cookieの保存処理
    foreach($post_data as $key => $atom) {
      if($atom !== '') {
        $this->getResponse()->setCookie("search[$key]", $atom, time() + 60 * 60 * 24 * 30);
      }
    }

    $this->redirect('client/show');
  }

  /**
   * 新規登録処理
   */
  public function executeNewSubmit()
  {
    $this->account_record = Mng::getAccount();
    $post_data = $this->getRequest()->getPostParameters();
    Client::saveNewClient($post_data);

    $this->redirect('client/show');
  }

  /**
   * レコードの削除処理
   */
  public function executeDeleteRecordSubmit()
  {
    $post_data = $this->getRequest()->getPostParameters();

    $account_record = Mng::getAccount();
    if ($account_record['authority_group_id'] == 1) {
      Client::deleteStoreRecord($post_data['store_id']);
    }

    $this->redirect('client/show');
  }

}
