<div id="wrapper">

  <!-- Sidebar -->
  <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="<?= url_for('client/show') ?>">顧客管理ページ</a>
    </div>

    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse navbar-ex1-collapse">
      <ul class="nav navbar-nav side-nav">
      </ul>

      <ul class="nav navbar-nav navbar-right navbar-user">
        <li class="dropdown messages-dropdown">
          <a href="<?= url_for('client/show') ?>" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-envelope"></i> 企業詳細ページ </b></a>
        </li>
        <li class="dropdown messages-dropdown">
          <a href="<?= url_for('client/search') ?>" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-envelope"></i> 検索ページ </b></a>
        </li>
        <li class="dropdown messages-dropdown">
          <a href="<?= url_for('client/new') ?>" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-envelope"></i> 新規企業登録 </b></a>
        </li>
        <li class="dropdown user-dropdown">
          <a href="<?= url_for('account/index') ?>" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-user"></i> Account <b class="caret"></b></a>
          <ul class="dropdown-menu">
            <li><a href="#"><i class="fa fa-user"></i> Profile</a></li>
            <li><a href="#"><i class="fa fa-envelope"></i> Inbox <span class="badge">7</span></a></li>
            <li><a href="#"><i class="fa fa-gear"></i> Settings</a></li>
            <li class="divider"></li>
            <li><a href="#"><i class="fa fa-power-off"></i> Log Out</a></li>
          </ul>
        </li>
        <li class="dropdown messages-dropdown">
          <a href="<?= url_for('top/logoutSubmit') ?>" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-envelope"></i> ログアウト </b></a>
        </li>
      </ul>
    </div><!-- /.navbar-collapse -->
  </nav>

  <div id="page-wrapper" class="page_bg_color">

    <form class="once_submit" method="POST" action="<?= url_for('client/newSubmit') ?>">

    <div class="row">
    
      <div class="col-md-6">
        <h4>企業・店舗情報&nbsp;&nbsp;<button type="submit" class="btn btn-default">新規登録</button></h4>
        <table class="table table-bordered table-condensed table_color">
          <tbody>
            <tr>
              <td><input type="radio" name="new_flag" value=1 checked>新規</td>
              <td><input type="radio" name="new_flag" value=0>既存</td>
              <td><input type="checkbox" name="expected_flag" value=1 >見込み</td>
              <td><input type="checkbox"  name="appointment_flag" value=1 >アポ</td>
            </tr>
            <tr>
              <td><input type="checkbox" name="call_again_flag" value=1>再コールチェック</td>
              <td>再コール時間</td>
              <td colspan="2"><? include_partial('datetimeForm', array('name' => 'call')) ?></td>
            </tr>
            <tr>
              <td><input type="checkbox" name="failure_receive_flag" value=1 >失注</td>
              <td><input type="checkbox"  name="phone_ban_flag" value=1 >コール禁止</td>
              <td><input type="checkbox"  name="send_document_flag" value=1 >メール資料送付済</td>
              <td><input type="checkbox"  name="send_document_fax_flag" value=1 >FAX資料送付済</td>
            </tr>
          </tbody>
        </table>
    
        <table class="table table-bordered table-condensed table_color">
          <tbody>
            <tr>
              <td class="cell_color">企業名ｶﾅ</td>
              <td colspan="3"><input type="text" class="kana" name="company_name_kana" style="width:100%" value=""></td>
            </tr>
            <tr>
              <td class="cell_color">企業名</td>
              <td colspan="3"><input type="text" name="company_name" style="width:100%" value=""></td>
            </tr>
            <tr>
              <td class="cell_color">代表者名ｶﾅ</td>
              <td><input type="text" name="president_kana" class="kana" style="width:100%" value=""></td>
              <td class="cell_color">先方担当ｶﾅ</td>
              <td><input type="text" name="person_in_charge_kana" class="kana" style="width:100%" value=""></td>
            </tr>
            <tr>
              <td class="cell_color">代表者名</td>
              <td><input type="text" name="president" style="width:100%" value=""></td>
              <td class="cell_color">先方担当者名</td>
              <td><input type="text" name="person_in_charge" style="width:100%" value=""></td>
            </tr>
            <tr>
              <td class="cell_color">営業担当者名</td>
              <td colspan="3"><input type="text" name="sales_representative" value=""></td>
            </tr>
            <tr>
              <td class="cell_color">企業住所</td>
              <td colspan="3"><input type="text" name="company_address" style="width:100%" value=""></td>
            </tr>
            <tr>
              <td class="cell_color">企業TEL</td>
              <td><input type="text" name="company_tel" style="width:100%" value=""></td>
              <td class="cell_color">携帯番号</td>
              <td><input type="text" name="company_mobile_phone" style="width:100%" value=""></td>
            </tr>
            <tr>
              <td class="cell_color">企業FAX</td>
              <td colspan="3"><input type="text" name="company_fax" style="width:100%" value=""></td>
            </tr>
            <tr>
              <td class="cell_color">店舗名</td>
              <td colspan="3"><input type="text" name="store_name" style="width:100%" value=""></td>
            </tr>
            <tr>
              <td class="cell_color">店舗住所</td>
              <td colspan="3"><input type="text" name="store_address" style="width:100%" value=""></td>
            </tr>
            <tr>
              <td class="cell_color">店舗TEL</td>
              <td><input type="text" name="store_tel" style="width:100%" value=""></td>
              <td class="cell_color">店舗FAX</td>
              <td><input type="text" name="store_fax" style="width:100%" value=""></td>
            </tr>
            <tr>
              <td class="cell_color">地方</td>
              <td><input type="text" name="region" style="width:100%" value=""></td>
              <td class="cell_color">都道府県</td>
              <td><input type="text" name="prefecture" style="width:100%" value=""></td>
            </tr>
            <tr>
              <td class="cell_color">メールアドレス</td>
              <td colspan="3"><input type="text" name="mail" style="width:100%" value=""></td>
            </tr>
            <tr>
              <td class="cell_color">URL</td>
              <td colspan="3"><input type="text" name="url" style="width:100%" value=""></td>
            </tr>
            <tr>
              <td class="cell_color">業種ジャンル</td>
              <td><input type="text" name="genre" style="width:100%" value=""></td>
              <td class="cell_color">店舗数</td>
              <td><input type="text" name="store_count" style="width:100%" value=""></td>
            </tr>
            <tr>
              <td colspan="4"><textarea name="memo" style="width:100%" rows="3"></textarea></td>
            </tr>
          </tbody>
        </table>
    
        <h4>提案商材</h4>
        <table class="table table-bordered table-condensed table_color">
          <tbody>
            <tr>
              <td><input type="checkbox" name="pp_gochipon" value=1>ごちぽん</td>
              <td>地域：<input type="text" name="pp_region" value=""></td>
              <td>都道府県：<input type="text" name="pp_prefecture" value=""></td>
            </tr>
            <tr>
              <td><input type="checkbox" name="pp_application" value=1>アプリ</td>
              <td colspan="2"><input type="checkbox" name="pp_possession_flag" value=1>自社アプリあり</td>
            </tr>
            <tr>
              <td colspan="3"><input type="checkbox" name="pp_addnet" value=1>アドネットワーク広告</td>
            </tr>
          </tbody>
        </table>
      </div>
    
      <div class="col-md-6">
        <h4>コール一覧</h4>
        <table class="table table-bordered table-condensed table_color">
          <tbody>
            <tr>
              <td><input type="radio" name="call_target" value="0">企業</td>
              <td><input type="radio" name="call_target" value="1">店舗</td>
            </tr>
            <tr>
              <td colspan="2">コール内容詳細</td>
            </tr>
            <tr>
              <td colspan="2"><textarea name="call_detail" style="width:100%" rows="3"></textarea></td>
            </tr>
          </tbody>
        </table>

        <h4>訪問一覧</h4>
        <table class="table table-bordered table-condensed table_color">
          <tbody>
            <tr>
              <td><input type="radio" name="visit_target" value="0">企業</td>
              <td><input type="radio" name="visit_target" value="1">店舗</td>
            </tr>
            <td>状態：
              <select name="status_num">
                <option value="0"><?= Client::getStatusName(0) ?></option>
                <option value="1"><?= Client::getStatusName(1) ?></option>
                <option value="2"><?= Client::getStatusName(2) ?></option>
                <option value="3"><?= Client::getStatusName(3) ?></option>
                <option value="4"><?= Client::getStatusName(4) ?></option>
                <option value="5"><?= Client::getStatusName(5) ?></option>
              </select>
            </td>
            <td>アポ：
              <select name="appointment_type">
                <option value="0"><?= Client::getProductName(0) ?></option>
                <option value="1"><?= Client::getProductName(1) ?></option>
                <option value="2"><?= Client::getProductName(2) ?></option>
                <option value="3"><?= Client::getProductName(3) ?></option>
              </select>
            </td>
            <tr>
              <td colspan="2">訪問内容詳細</td>
            </tr>
            <tr>
              <td colspan="2"><textarea name="visit_detail" style="width:100%" rows="3"></textarea></td>
            </tr>
          </tbody>
        </table>

        <h4>取引商材（ご契約済み）</h4>
        <table class="table table-bordered table-condensed table_color">
          <tbody>
            <tr>
              <td><input type="checkbox" name="dp_gochipon" value=1>ごちぽん</td>
              <td>地域：<input type="text" name="dp_region" value=""></td>
              <td>都道府県：<input type="text" name="dp_prefecture" value=""></td>
            </tr>
            <tr>
              <td colspan="3"><input type="checkbox" name="dp_application" value=1>アプリ</td>
            </tr>
            <tr>
              <td colspan="3"><input type="checkbox" name="dp_addnet" value=1>アドネットワーク広告</td>
            </tr>
          </tbody>
        </table>
    
        <h4>請求書先</h4>
        <table class="table table-bordered table-condensed table_color">
          <tbody>
            <tr>
              <td><input type="radio" name="bd_target" value=0>企業</td>
              <td><input type="radio" name="bd_target" value=1>店舗</td>
              <td><input type="radio" name="bd_target" value=2>その他</td>
            </tr>
            <tr>
              <td>宛名</td>
              <td colspan="2"><input type="text" name="bd_name" style="width:100%" value=""></td>
            </tr>
            <tr>
              <td>住所</td>
              <td colspan="2"><input type="text" name="bd_address" style="width:100%" value=""></td>
            </tr>
            <tr>
              <td>電話</td>
              <td colspan="2"><input type="text" name="bd_tel" value=""></td>
            </tr>
          </tbody>
        </table>

        <h4>ご契約内容</h4>
        <table class="table table-bordered table-condensed table_color">
          <tbody>
            <tr>
              <td>ご契約日</td>
              <td><? include_partial('dateForm', array('name' => 'contract')) ?></td>
            </tr>
            <tr>
              <td><input type="radio" name="contract_target" value=0>企業</td>
              <td><input type="radio" name="contract_target" value=1>店舗</td>
            </tr>
            <tr>
              <td>商材名</td>
              <td>
                <select name="contract_product_name">
                  <option value="">--</option>
                  <option value=1>ごちぽん</option>
                  <option value=2>アプリ</option>
                  <option value=3>アドネットワーク広告</option>
                </select>
              </td>
            </tr>
            <tr>
              <td>初期費用</td>
              <td><input type="number" name="initial_cost" value="">&nbsp;円</td>
            </tr>
            <tr>
              <td><input type="radio" name="period_flag" value=0>年間</td>
              <td><input type="radio" name="period_flag" value=1>月額</td>
            </tr>
            <tr>
              <td>金額</td>
              <td><input type="number" name="payment" value="">&nbsp;円</td>
            </tr>
            <tr>
              <td>契約期間</td>
              <td><? include_partial('dateForm', array('name' => 'start')) ?>〜&nbsp;<? include_partial('dateForm', array('name' => 'end')) ?></td>
            </tr>
            <tr>
              <td colspan="2"><textarea name="contract_detail" style="width:100%" rows="3"></textarea></td>
            </tr>
          </tbody>
        </table>
      </div>
    </div>

  </form>
  </div><!-- /#page-wrapper -->

</div><!-- /#wrapper -->
<script>
$(function(){
  $('.once_submit').disableOnSubmit();
});
</script>