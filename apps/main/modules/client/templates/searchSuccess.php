<div id="wrapper">

  <!-- Sidebar -->
  <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="<?= url_for('client/show') ?>">顧客管理ページ</a>
    </div>

    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse navbar-ex1-collapse">
      <ul class="nav navbar-nav side-nav">
      </ul>

      <ul class="nav navbar-nav navbar-right navbar-user">
        <li class="dropdown messages-dropdown">
          <a href="<?= url_for('client/show') ?>" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-envelope"></i> 企業詳細ページ </b></a>
        </li>
        <li class="dropdown messages-dropdown">
          <a href="<?= url_for('client/search') ?>" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-envelope"></i> 検索ページ </b></a>
        </li>
        <li class="dropdown messages-dropdown">
          <a href="<?= url_for('client/new') ?>" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-envelope"></i> 新規企業登録 </b></a>
        </li>
        <li class="dropdown user-dropdown">
          <a href="<?= url_for('account/index') ?>" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-user"></i> Account <b class="caret"></b></a>
          <ul class="dropdown-menu">
            <li><a href="#"><i class="fa fa-user"></i> Profile</a></li>
            <li><a href="#"><i class="fa fa-envelope"></i> Inbox <span class="badge">7</span></a></li>
            <li><a href="#"><i class="fa fa-gear"></i> Settings</a></li>
            <li class="divider"></li>
            <li><a href="#"><i class="fa fa-power-off"></i> Log Out</a></li>
          </ul>
        </li>
        <li class="dropdown messages-dropdown">
          <a href="<?= url_for('top/logoutSubmit') ?>" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-envelope"></i> ログアウト </b></a>
        </li>
      </ul>
    </div><!-- /.navbar-collapse -->
  </nav>

  <div id="page-wrapper" class="page_bg_color">
  <form method="POST" action="<?= url_for('client/searchSubmit') ?>">

    <div class="row">
      <div class="col-lg-12">
        <h1>検索ページ&nbsp;&nbsp;<button type="submit" class="btn btn-default">検索</button></h1>
      </div>
    </div>

    <div class="row">
    
      <div class="col-md-6">
          <h4>企業・店舗情報</h4>
          <table class="table table-bordered table-condensed table_color">
            <tbody>
              <tr>
                <td><input type="checkbox" name="new_flag" value=1 checked>新規</td>
                <td><input type="checkbox" name="exist_flag" value=1 checked>既存</td>
                <td><input type="checkbox" name="appointment_flag" value=1>アポ</td>
                <td><input type="checkbox" name="call_again_sort_flag" value=1>再コール日順</td>
              </tr>
              <tr>
                <td><input type="checkbox" name="call_again_flag" value=1>再コールチェック</td>
                <td>再コール日</td>
                <td colspan="2"><? include_partial('dateForm', array('name' => 'call')) ?></td>
              </tr>
              <tr>
                <td colspan="4"><input type="checkbox" name="phone_ban_flag" value=1>コール禁止</td>
              </tr>
              <tr>
                <td><input type="checkbox" name="send_document_true_flag" value=1>メール資料送付済</td>
                <td><input type="checkbox" name="send_document_false_flag" value=0>メール未送付</td>
                <td><input type="checkbox" name="send_document_fax_true_flag" value=1>FAX資料送付済</td>
                <td><input type="checkbox" name="send_document_fax_false_flag" value=0>FAX未送付</td>
              </tr>
              <tr>
                <td><input type="checkbox" name="expected_true_flag" value=1>見込みあり</td>
                <td><input type="checkbox" name="expected_false_flag" value=1>見込みなし</td>
                <td><input type="checkbox" name="failure_receive_true_flag" value=1>失注あり</td>
                <td><input type="checkbox" name="failure_receive_false_flag" value=1>失注なし</td>
              </tr>
            </tbody>
          </table>
    
          <table class="table table-bordered table-condensed table_color">
            <tbody>
              <tr>
                <td class="cell_color">企業名ｶﾅ</td>
                <td colspan="3"><input type="text" name="company_name_kana" class="kana" value="" style="width:100%"></td>
              </tr>
              <tr>
                <td class="cell_color">企業名</td>
                <td colspan="3"><input type="text" name="company_name" value="" style="width:100%"></td>
              </tr>
              <tr>
                <td class="cell_color">代表者名ｶﾅ</td>
                <td><input type="text" name="president_kana" class="kana" value="" style="width:100%"></td>
                <td class="cell_color">先方担当ｶﾅ</td>
                <td><input type="text" name="person_in_charge_kana" class="kana" value="" style="width:100%"></td>
              </tr>
              <tr>
                <td class="cell_color">代表者名</td>
                <td><input type="text" name="president" value="" style="width:100%"></td>
                <td class="cell_color">先方担当</td>
                <td><input type="text" name="person_in_charge" value="" style="width:100%"></td>
              </tr>
              <tr>
                <td class="cell_color">営業担当</td>
                <td colspan="3"><input type="text" name="sales_representative" value=""></td>
              </tr>
              <tr>
                <td class="cell_color">企業住所</td>
                <td colspan="3"><input type="text" name="company_address" value="" style="width:100%"></td>
              </tr>
              <tr>
                <td class="cell_color">企業TEL</td>
                <td><input type="text" name="company_tel" value="" style="width:100%"></td>
                <td class="cell_color">携帯番号</td>
                <td><input type="text" name="company_mobile_phone" value="" style="width:100%"></td>
              </tr>
              <tr>
                <td class="cell_color">企業FAX</td>
                <td colspan="3"><input type="text" name="company_fax" value="" style="width:100%"></td>
              </tr>
              <tr>
                <td class="cell_color">店舗名</td>
                <td colspan="3"><input type="text" name="store_name" value="" style="width:100%"></td>
              </tr>
              <tr>
                <td class="cell_color">店舗住所</td>
                <td colspan="3"><input type="text" name="store_address" value="" style="width:100%"></td>
              </tr>
              <tr>
                <td class="cell_color">店舗TEL</td>
                <td><input type="text" name="store_tel" value="" style="width:100%"></td>
                <td class="cell_color">店舗FAX</td>
                <td><input type="text" name="store_fax" value="" style="width:100%"></td>
              </tr>
              <tr>
                <td class="cell_color">地方</td>
                <td><input type="text" name="region" value="" style="width:100%"></td>
                <td class="cell_color">都道府県</td>
                <td><input type="text" name="prefecture" value="" style="width:100%"></td>
              </tr>
              <tr>
                <td class="cell_color">メールアドレス</td>
                <td colspan="3"><input type="text" name="mail" value="" style="width:100%"></td>
              </tr>
              <tr>
                <td class="cell_color">URL</td>
                <td colspan="3"><input type="text" name="url" value="" style="width:100%"></td>
              </tr>
              <tr>
                <td class="cell_color">業種ジャンル</td>
                <td><input type="text" name="genre" style="width:100%" value=""></td>
                <td class="cell_color">店舗数</td>
                <td><input type="text" name="store_count" style="width:100%" value=""></td>
              </tr>
              <tr>
                <td colspan="4"><textarea name="memo" style="width:100%" rows="3"></textarea></td>
              </tr>
            </tbody>
          </table>
      </div>
    
      <div class="col-md-6">
        <h4>提案商材</h4>
        <table class="table table-bordered table-condensed table_color">
          <tbody>
            <tr>
              <td><input type="checkbox" name="pp_gochipon" value=1>ごちぽん</td>
              <td>地域：<input type="text" name="pp_region" value=""></td>
              <td>都道府県：<input type="text" name="pp_prefecture" value=""></td>
            </tr>
            <tr>
              <td><input type="checkbox" name="pp_application" value=1>アプリ</td>
              <td colspan="2"><input type="checkbox" name="pp_possession_flag" value=1>自社アプリあり</td>
            </tr>
            <tr>
              <td colspan="3"><input type="checkbox" name="pp_addnet" value=1>アドネットワーク広告</td>
            </tr>
          </tbody>
        </table>

        <h4>コール一覧</h4>
        <table class="table table-bordered table-condensed table_color">
          <tbody>
            <tr>
              <td>コール内容詳細</td>
              <td>営業担当：<input type="text" name="call_caller" value=""></td>
            </tr>
            <tr>
              <td colspan="2"><textarea name="call_detail" style="width:100%"></textarea></td>
            </tr>
          </tbody>
        </table>
      </div>
    </div>
  </form>
  </div><!-- /#page-wrapper -->

</div><!-- /#wrapper -->