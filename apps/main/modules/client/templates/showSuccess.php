<div id="wrapper">

  <!-- Sidebar -->
  <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="<?= url_for('client/show?list_num=' . $list_num) ?>">顧客管理ページ</a>
    </div>

    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse navbar-ex1-collapse">
      <ul class="nav navbar-nav side-nav">
        <div align="center">
          <? if ($pager_id > 1) { ?>
            <a href="<?= url_for('client_show', array('list_num' => 0, 'pager_id' => $pager_id - 1)) ?>">前へ</a>
          <? } else { ?>
            <a>前へ</a>
          <? } ?>
          <span><font color="white">&nbsp;<?= $pager_id ?> /<?= $max_pager_id ?>&nbsp;</font></span>
          <? if ($next_flag) { ?>
            <a href="<?= url_for('client_show', array('list_num' => 0, 'pager_id' => $pager_id + 1)) ?>">次へ</a>
          <? } else { ?>
            <a>次へ</a>
          <? } ?>
        </div>
        <? $num = 0; ?>
        <? foreach($store_records as $store_record ) { ?>
          <? if($num == $list_num) { ?>
            <li class="active">
              <a href="<?= url_for('client_show', array('list_num' => $num, 'pager_id' => $pager_id)) ?>">
                <i class="fa fa-dashboard"></i>
                <?= $store_record['company_name'] ?>
              </a>
            </li>
          <? } else { ?>
            <li>
              <a href="<?= url_for('client_show', array('list_num' => $num, 'pager_id' => $pager_id)) ?>">
                <i class="fa fa-dashboard"></i>
                <?= $store_record['company_name'] ?>
              </a>
            </li>
          <? } ?>
          <? $num++; ?>
        <? } ?>
      </ul>

      <ul class="nav navbar-nav navbar-right navbar-user">
        <li class="dropdown messages-dropdown">
          <a href="<?= url_for('client/show') ?>" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-envelope"></i> 企業詳細ページ </b></a>
        </li>
        <li class="dropdown messages-dropdown">
          <a href="<?= url_for('client/search') ?>" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-envelope"></i> 検索ページ </b></a>
        </li>
        <li class="dropdown messages-dropdown">
          <a href="<?= url_for('client/new') ?>" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-envelope"></i> 新規企業登録 </b></a>
        </li>
        <li class="dropdown user-dropdown">
          <a href="<?= url_for('account/index') ?>" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-user"></i> Account <b class="caret"></b></a>
          <ul class="dropdown-menu">
            <li><a href="#"><i class="fa fa-user"></i> Profile</a></li>
            <li><a href="#"><i class="fa fa-envelope"></i> Inbox <span class="badge">7</span></a></li>
            <li><a href="#"><i class="fa fa-gear"></i> Settings</a></li>
            <li class="divider"></li>
            <li><a href="#"><i class="fa fa-power-off"></i> Log Out</a></li>
          </ul>
        </li>
        <li class="dropdown messages-dropdown">
          <a href="<?= url_for('top/logoutSubmit') ?>" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-envelope"></i> ログアウト </b></a>
        </li>
      </ul>
    </div><!-- /.navbar-collapse -->
  </nav>

  <div id="page-wrapper" class="page_bg_color">

    <div class="row">
      <div class="col-lg-12">
      </div>
    </div>

    <div class="row">
    
      <div class="col-md-6">
        <form class="once_submit" method="POST" action="<?= url_for('client/changeSubmit') ?>">
          <input type="hidden" name="store_id" value="<?= $show_record['id'] ?>">
          <h4>企業・店舗情報&nbsp;&nbsp;<button type="submit" class="btn btn-default">保存</button>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            <? if($list_num > 0) { ?>
              <button type="button" onclick="location.href='<?= url_for('client_show', array('list_num' => $list_num - 1, 'pager_id' => $pager_id)) ?>'" class="btn btn-default">前のページへ</button>
            <? } else { ?>
              <? if($pager_id > 1) { ?>
                <button type="button" onclick="location.href='<?= url_for('client_show', array('list_num' => $per_page - 1, 'pager_id' => $pager_id - 1)) ?>'" class="btn btn-default">前のページへ</button>
              <? } else { ?>
                <button type="button" onclick="location.href='<?= url_for('client_show', array('list_num' => $list_num, 'pager_id' => $pager_id)) ?>'" class="btn btn-default">前のページへ</button>
              <? } ?>
            <? } ?>
            <? if($list_num < count($store_records) - 1) { ?>
              &nbsp;&nbsp;<button type="button" onclick="location.href='<?= url_for('client_show', array('list_num' => $list_num + 1, 'pager_id' => $pager_id)) ?>'" class="btn btn-default">次のページへ</button>
            <? } else { ?>
              <? if($pager_id < $max_pager_id) { ?>
                &nbsp;&nbsp;<button type="button" onclick="location.href='<?= url_for('client_show', array('list_num' => 0, 'pager_id' => $pager_id + 1)) ?>'" class="btn btn-default">次のページへ</button>
              <? } else { ?>
                &nbsp;&nbsp;<button type="button" onclick="location.href='<?= url_for('client_show', array('list_num' => $list_num, 'pager_id' => $pager_id)) ?>'" class="btn btn-default">次のページへ</button>
              <? } ?>
            <? } ?>
          </h4>
          <table class="table table-bordered table-condensed table_color">
            <tbody>
              <tr>
                <? if($show_record['new_flag'] == true) { ?>
                  <td><input type="radio" name="new_flag" value=1 checked>新規</td>
                  <td><input type="radio" name="new_flag" value=0>既存</td>
                <? } else { ?>
                  <td><input type="radio" name="new_flag" value=1>新規</td>
                  <td><input type="radio" name="new_flag" value=0 checked>既存</td>
                <? } ?>
                <td><input type="checkbox" name="expected_flag" value=1 <? if($show_record['expected_flag'] == true) { echo 'checked'; } ?>>見込み</td>
                <td><input type="checkbox" name="appointment_flag" value=1 <? if($show_record['appointment_flag'] == true) { echo 'checked'; } ?>>アポ</td>
              </tr>
              <tr>
                <td><input type="checkbox" name="call_again_flag" value=1 <? if($show_record['call_again_flag'] == true) { echo 'checked'; } ?>>再コールチェック</td>
                <td>再コール時間</td>
                <? list($year, $month, $day, $hour, $min, $sec) = preg_split("/[-: ]/", $show_record['call_schedule']); ?>
                <td colspan="2"><? include_partial('datetimeExistForm', array('name' => 'call', 'year' => $year, 'month' => $month, 'day' => $day, 'hour' => $hour, 'min' => $min)) ?></td>
              </tr>
              <tr>
                <td><input type="checkbox" name="failure_receive_flag" value=1 <? if($show_record['failure_receive_flag'] == true) { echo 'checked'; } ?>>失注</td>
                <td><input type="checkbox" name="phone_ban_flag" value=1 <? if($show_record['phone_ban_flag'] == true) { echo 'checked'; } ?>>コール禁止</td>
                <td><input type="checkbox" name="send_document_flag" value=1 <? if($show_record['send_document_flag'] == true) { echo 'checked'; } ?>>メール資料送付済</td>
                <td><input type="checkbox" name="send_document_fax_flag" value=1 <? if($show_record['send_document_fax_flag'] == true) { echo 'checked'; } ?>>FAX資料送付済</td>
              </tr>
            </tbody>
          </table>
    
          <table class="table table-bordered table-condensed table_color">
            <tbody>
              <tr>
                <td class="cell_color">企業名ｶﾅ</td>
                <td colspan="3"><input type="text" class="kana" name="company_name_kana" style="width:100%" value="<?= $show_record['company_name_kana'] ?>"></td>
              </tr>
              <tr>
                <td class="cell_color">企業名</td>
                <td colspan="3"><input type="text" name="company_name" style="width:100%" value="<?= $show_record['company_name'] ?>"></td>
              </tr>
              <tr>
                <td class="cell_color">代表者名ｶﾅ</td>
                <td><input type="text" name="president_kana" class="kana" style="width:100%" value="<?= $show_record['president_kana'] ?>" <? if ($show_record['president_kana'] != '') {echo $edit_flag;} ?>></td>
                <td class="cell_color">先方担当ｶﾅ</td>
                <td><input type="text" name="person_in_charge_kana" class="kana" style="width:100%" value="<?= $show_record['person_in_charge_kana'] ?>"></td>
              </tr>
              <tr>
                <td class="cell_color">代表者名</td>
                <td><input type="text" name="president" style="width:100%" value="<?= $show_record['president'] ?>" <? if ($show_record['president'] != '') {echo $edit_flag;} ?>></td>
                <td class="cell_color">先方担当</td>
                <td><input type="text" name="person_in_charge" style="width:100%" value="<?= $show_record['person_in_charge'] ?>"></td>
              </tr>
              <tr>
                <td class="cell_color">営業担当</td>
                <td colspan="3"><input type="text" name="sales_representative" value="<?= $show_record['sales_representative'] ?>"></td>
              </tr>
              <tr>
                <td class="cell_color">企業住所</td>
                <td colspan="3"><input type="text" name="company_address" style="width:100%" value="<?= $show_record['company_address'] ?>" <? if ($show_record['company_address'] != '') {echo $edit_flag;} ?>></td>
              </tr>
              <tr>
                <td class="cell_color">企業TEL</td>
                <td><input type="text" name="company_tel" style="width:100%" value="<?= $show_record['company_tel'] ?>"></td>
                <td class="cell_color">携帯番号</td>
                <td><input type="text" name="company_mobile_phone" style="width:100%" value="<?= $show_record['company_mobile_phone'] ?>"></td>
              </tr>
              <tr>
                <td class="cell_color">企業FAX</td>
                <td colspan="3"><input type="text" name="company_fax" style="width:100%" value="<?= $show_record['company_fax'] ?>" <? if ($show_record['company_fax'] != '') {echo $edit_flag;} ?>></td>
              </tr>
              <tr>
                <td class="cell_color">店舗名</td>
                <td colspan="3"><input type="text" name="store_name" style="width:100%" value="<?= $show_record['store_name'] ?>" <? if ($show_record['store_name'] != '') {echo $edit_flag;} ?>></td>
              </tr>
              <tr>
                <td class="cell_color">店舗住所</td>
                <td colspan="3"><input type="text" name="store_address" style="width:100%" value="<?= $show_record['store_address'] ?>" <? if ($show_record['store_address'] != '') {echo $edit_flag;} ?>></td>
              </tr>
              <tr>
                <td class="cell_color">店舗TEL</td>
                <td><input type="text" name="store_tel" style="width:100%" value="<?= $show_record['store_tel'] ?>"></td>
                <td class="cell_color">店舗FAX</td>
                <td><input type="text" name="store_fax" style="width:100%" value="<?= $show_record['store_fax'] ?>" <? if ($show_record['store_fax'] != '') {echo $edit_flag;} ?>></td>
              </tr>
              <tr>
                <td class="cell_color">地方</td>
                <td><input type="text" name="region" style="width:100%" value="<?= $show_record['region'] ?>" <? if ($show_record['region'] != '') {echo $edit_flag;} ?>></td>
                <td class="cell_color">都道府県</td>
                <td><input type="text" name="prefecture" style="width:100%" value="<?= $show_record['prefecture'] ?>" <? if ($show_record['prefecture'] != '') {echo $edit_flag;} ?>></td>
              </tr>
              <tr>
                <td class="cell_color">メール</td>
                <td colspan="3"><input type="text" name="mail" style="width:100%" value="<?= $show_record['mail'] ?>"></td>
              </tr>
              <tr>
                <td class="cell_color">URL</td>
                <td colspan="3"><input type="text" name="url" style="width:100%" value="<?= $show_record['url'] ?>"></td>
              </tr>
              <tr>
                <td class="cell_color">業種ジャンル</td>
                <td><input type="text" name="genre" style="width:100%" value="<?= $show_record['genre'] ?>" <? if ($show_record['genre'] != '') {echo $edit_flag;} ?>></td>
                <td class="cell_color">店舗数</td>
                <td><input type="text" name="store_count" style="width:100%" value="<?= $show_record['store_count'] ?>" <? if ($show_record['store_count'] != '') {echo $edit_flag;} ?>></td>
              </tr>
              <tr>
                <td colspan="4">
                  <textarea name="memo" style="width:100%" rows="3" <? if ($show_record['memo'] != '') {echo $edit_flag;} ?>><?= $show_record['memo'] ?></textarea>
                </td>
              </tr>
            </tbody>
          </table>
        </form>
    
        <h4>提案商材</h4>
        <form class="once_submit" method="POST" action="<?= url_for('client/ppChangeSubmit') ?>">
          <input type="hidden" name="store_id" value="<?= $show_record['id'] ?>">
          <table class="table table-bordered table-condensed table_color">
            <tbody>
              <? //FIXME: 強引に行っているので後で処理を修正 ?>
              <? $pp_gochipon = array(); ?>
              <? $pp_application = array(); ?>
              <? $pp_addnet = array(); ?>
              <? foreach($show_record['ProposalProduct'] as $atom) { ?>
                <?
                  switch ($atom['Product']['name']) {
                    case 'ごちぽん':
                      $pp_gochipon = $atom;
                      break;
                    case 'アプリ':
                      $pp_application = $atom;
                      break;
                    case 'アドネットワーク広告':
                      $pp_addnet = $atom;
                      break;
                  }
                ?>
              <? } ?>
              <tr>
                <? if (isset($pp_gochipon['id'])) { ?>
                  <td><input type="checkbox" name="pp_gochipon" value=1 onclick="<?= $check_edit ?>" checked>ごちぽん</td>
                  <td>地域：<input type="text" name="pp_region" value="<?= $pp_gochipon['region'] ?>" <?= $edit_flag; ?>></td>
                  <td>都道府県：<input type="text" name="pp_prefecture" value="<?= $pp_gochipon['prefecture'] ?>" <?= $edit_flag; ?>></td>
                  <input type="hidden" name="pp_gochipon_id" value="<?= $pp_gochipon['id'] ?>">
                <? } else { ?>
                  <td><input type="checkbox" name="pp_gochipon" value=1 onclick="<?= $check_edit ?>">ごちぽん</td>
                  <td>地域：<input type="text" name="pp_region" value="" <?= $edit_flag; ?>></td>
                  <td>都道府県：<input type="text" name="pp_prefecture" value="" <?= $edit_flag; ?>></td>
                <? } ?>
              </tr>
              <tr>
                <? if (isset($pp_application['id'])) { ?>
                  <td><input type="checkbox" name="pp_application" value=1 onclick="<?= $check_edit ?>" checked>アプリ</td>
                  <td colspan="2"><input type="checkbox" name="pp_possession_flag" value=1 onclick="<?= $check_edit ?>" <?= $pp_application['possession_flag'] ? 'checked' : '' ?>>自社アプリあり</td>
                  <input type="hidden" name="pp_application_id" value="<?= $pp_application['id'] ?>">
                <? } else { ?>
                  <td><input type="checkbox" name="pp_application" value=1 onclick="<?= $check_edit ?>">アプリ</td>
                  <td colspan="2"><input type="checkbox" name="pp_possession_flag" value=1 onclick="<?= $check_edit ?>">自社アプリあり</td>
                <? } ?>
              </tr>
              <tr>
                <? if (isset($pp_addnet['id'])) { ?>
                  <td colspan="3"><input type="checkbox" name="pp_addnet" value=1 onclick="<?= $check_edit ?>" checked>アドネットワーク広告</td>
                  <input type="hidden" name="pp_addnet_id" value="<?= $pp_addnet['id'] ?>">
                <? } else { ?>
                  <td colspan="3"><input type="checkbox" name="pp_addnet" value=1 onclick="<?= $check_edit ?>">アドネットワーク広告</td>
                <? } ?> 
              </tr>
              <tr>
                <td colspan="3"><button type="submit" class="btn btn-default">変更</button></td>
              </tr>
            </tbody>
          </table>
        </form>
      </div>
    
      <div class="col-md-6">
        <h4>コール一覧</h4>
        <div class="call_scroll call_bg_color">
          <form class="once_submit" method="POST" action="<?= url_for('client/callAddSubmit') ?>">
            <input type="hidden" name="store_id" value="<?= $show_record['id'] ?>">
            <table class="table table-bordered table-condensed table_color">
              <tbody>
                <tr>
                  <td><input type="radio" name="call_target" value="0" checked>企業</td>
                  <td><input type="radio" name="call_target" value="1">店舗</td>
                </tr>
                <tr>
                  <td colspan="2">コール内容詳細</td>
                </tr>
                <tr>
                  <td colspan="2"><textarea name="call_detail" style="width:100%" rows="3"></textarea><button type="submit" class="btn btn-default">追加</button></td>
                </tr>
              </tbody>
            </table>
          </form>
          <? $call_info_count = count($show_record['CallInfo']) ?>
          <? $call_infos = array() ?>
          <? for ($i = $call_info_count - 1; $i >= 0; $i--) { ?>
            <? $call_infos[] = $show_record['CallInfo'][$i] ?>
          <? } ?>
          <? foreach($call_infos as $atom) { ?>
            <form class="once_submit" method="POST" action="<?= url_for('client/callChangeSubmit') ?>">
              <table class="table table-bordered table-condensed table_color">
                <input type="hidden" name="id" value="<?= $atom['id'] ?>">
                <tbody>
                  <tr>
                    <td>コール日時</td>
                    <? list($year, $month, $day, $hour, $min, $sec) = preg_split("/[-: ]/", $atom['call_date']); ?>
                    <td><? include_partial('datetimeExistForm', array('name' => 'call', 'year' => $year, 'month' => $month, 'day' => $day, 'hour' => $hour, 'min' => $min)) ?></td>
                  </tr>
                  <tr>
                    <td><input type="checkbox" name="call_target" value="0" <? if((int) $atom['target'] === 0) { echo 'checked'; } ?>>企業</td>
                    <td><input type="checkbox" name="call_target" value="1" <? if((int) $atom['target'] === 1) { echo 'checked'; } ?>>店舗</td>
                  </tr>
                  <tr>
                    <td>コール内容詳細</td>
                    <td>営業担当：<input type="text" name="call_caller" value="<?= $atom['caller'] ?>"></td>
                  </tr>
                  <tr>
                    <td colspan="2"><textarea name="call_detail" style="width:100%"><?= $atom['detail'] ?></textarea><button type="submit" class="btn btn-default">変更</button></td>
                  </tr>
                </tbody>
              </table>
            </form>
          <? } ?>
        </div>

        <h4>訪問一覧</h4>
        <div class="call_scroll call_bg_color">
          <form class="once_submit" method="POST" action="<?= url_for('client/visitAddSubmit') ?>">
            <input type="hidden" name="store_id" value="<?= $show_record['id'] ?>">
            <table class="table table-bordered table-condensed table_color">
              <tbody>
                <tr>
                  <td><input type="radio" name="visit_target" value="0" checked>企業</td>
                  <td><input type="radio" name="visit_target" value="1">店舗</td>
                </tr>
                <tr>
                  <td>状態：
                    <select name="status_num">
                      <option value="0"><?= Client::getStatusName(0) ?></option>
                      <option value="1"><?= Client::getStatusName(1) ?></option>
                      <option value="2"><?= Client::getStatusName(2) ?></option>
                      <option value="3"><?= Client::getStatusName(3) ?></option>
                      <option value="4"><?= Client::getStatusName(4) ?></option>
                      <option value="5"><?= Client::getStatusName(5) ?></option>
                    </select>
                  </td>
                  <td>アポ：
                    <select name="appointment_type">
                      <option value="0"><?= Client::getProductName(0) ?></option>
                      <option value="1"><?= Client::getProductName(1) ?></option>
                      <option value="2"><?= Client::getProductName(2) ?></option>
                      <option value="3"><?= Client::getProductName(3) ?></option>
                    </select>
                  </td>
                </tr>
                <tr>
                  <td colspan="2">訪問内容詳細</td>
                </tr>
                <tr>
                  <td colspan="2"><textarea name="visit_detail" style="width:100%" rows="3"></textarea><button type="submit" class="btn btn-default">追加</button></td>
                </tr>
              </tbody>
            </table>
          </form>
          <? $visit_info_count = count($show_record['VisitInfo']) ?>
          <? $visit_infos = array() ?>
          <? for ($i = $visit_info_count - 1; $i >= 0; $i--) { ?>
            <? $visit_infos[] = $show_record['VisitInfo'][$i] ?>
          <? } ?>
          <? foreach($visit_infos as $atom) { ?>
            <form class="once_submit" method="POST" action="<?= url_for('client/visitChangeSubmit') ?>">
              <table class="table table-bordered table-condensed table_color">
                <input type="hidden" name="id" value="<?= $atom['id'] ?>">
                <tbody>
                  <tr>
                    <td>訪問日時</td>
                    <? list($year, $month, $day, $hour, $min, $sec) = preg_split("/[-: ]/", $atom['visit_date']); ?>
                    <td><? include_partial('datetimeExistForm', array('name' => 'visit', 'year' => $year, 'month' => $month, 'day' => $day, 'hour' => $hour, 'min' => $min)) ?></td>
                  </tr>
                  <tr>
                    <td><input type="checkbox" name="visit_target" value="0" <? if((int) $atom['target'] === 0) { echo 'checked'; } ?>>企業</td>
                    <td><input type="checkbox" name="visit_target" value="1" <? if((int) $atom['target'] === 1) { echo 'checked'; } ?>>店舗</td>
                  </tr>
                  <tr>
                    <td>状態：
                      <select name="status_num">
                        <option value="<?= $atom['status_num'] ?>"><?= Client::getStatusName($atom['status_num']) ?></option>
                        <option value="0"><?= Client::getStatusName(0) ?></option>
                        <option value="1"><?= Client::getStatusName(1) ?></option>
                        <option value="2"><?= Client::getStatusName(2) ?></option>
                        <option value="3"><?= Client::getStatusName(3) ?></option>
                        <option value="4"><?= Client::getStatusName(4) ?></option>
                        <option value="5"><?= Client::getStatusName(5) ?></option>
                      </select>
                    </td>
                    <td>アポ：
                      <select name="appointment_type">
                      <option value="<?= $atom['appointment_type'] ?>"><?= Client::getProductName($atom['appointment_type']) ?></option>
                        <option value="0"><?= Client::getProductName(0) ?></option>
                        <option value="1"><?= Client::getProductName(1) ?></option>
                        <option value="2"><?= Client::getProductName(2) ?></option>
                        <option value="3"><?= Client::getProductName(3) ?></option>
                      </select>
                    </td>
                  </tr>
                  <tr>
                    <td colspan="2">訪問内容詳細</td>
                  </tr>
                  <tr>
                    <td colspan="2"><textarea name="visit_detail" style="width:100%"><?= $atom['detail'] ?></textarea><button type="submit" class="btn btn-default">変更</button></td>
                  </tr>
                </tbody>
              </table>
            </form>
          <? } ?>
        </div>

        <h4>取引商材（ご契約済み）</h4>
        <table class="table table-bordered table-condensed table_color">
          <tbody>
            <? //FIXME: 強引に行っているので後で処理を修正 ?>
            <? $dp_gochipon = array(); ?>
            <? $dp_application = array(); ?>
            <? $dp_addnet = array(); ?>
            <? foreach($show_record['DealProduct'] as $atom) { ?>
              <?
                switch ($atom['Product']['name']) {
                  case 'ごちぽん':
                    $dp_gochipon = $atom;
                    break;
                  case 'アプリ':
                    $dp_application = $atom;
                    break;
                  case 'アドネットワーク広告':
                    $dp_addnet = $atom;
                    break;
                }
              ?>
            <? } ?>
            <tr>
              <? if (isset($dp_gochipon['id'])) { ?>
                <td><input type="checkbox" onclick="return false" name="dp_gochipon" value=1 checked>ごちぽん</td>
                <td><?= $dp_gochipon['region'] ?></td>
                <td><?= $dp_gochipon['prefecture'] ?></td>
              <? } else { ?>
                <td><input type="checkbox" onclick="return false" name="dp_gochipon" value=1 >ごちぽん</td>
                <td></td>
                <td></td>
              <? } ?>
            </tr>
            <tr>
              <? if (isset($dp_application['id'])) { ?>
                <td colspan="3"><input type="checkbox" onclick="return false" name="dp_application" value=1 checked>アプリ</td>
              <? } else { ?>
                <td colspan="3"><input type="checkbox" onclick="return false" name="dp_application" value=1>アプリ</td>
              <? } ?>
            </tr>
            <tr>
              <? if (isset($dp_addnet['id'])) { ?>
                <td colspan="3"><input type="checkbox" onclick="return false" name="dp_addnet" value=1 checked>アドネットワーク広告</td>
              <? } else { ?>
                <td colspan="3"><input type="checkbox" onclick="return false" name="dp_addnet" value=1>アドネットワーク広告</td>
              <? } ?>
            </tr>
          </tbody>
        </table>
    
        <? if(isset($show_record['Contract'][0])) { ?>
          <? if(isset($show_record['Contract'][0]['BillingDestination'][0])) { ?>
            <h4>請求書先</h4>
            <table class="table table-bordered table-condensed table_color">
              <tbody>
                <tr>
                  <td><input type="checkbox" onclick="return false" name="example" value="サンプル" <? if((int) $show_record['Contract'][0]['BillingDestination'][0]['target'] === 0) { echo 'checked'; } ?>>企業</td>
                  <td><input type="checkbox" onclick="return false" name="example" value="サンプル" <? if((int) $show_record['Contract'][0]['BillingDestination'][0]['target'] === 1) { echo 'checked'; } ?>>店舗</td>
                  <td><input type="checkbox" onclick="return false" name="example" value="サンプル" <? if((int) $show_record['Contract'][0]['BillingDestination'][0]['target'] === 2) { echo 'checked'; } ?>>その他</td>
                </tr>
                <tr>
                  <td>宛名</td>
                  <td colspan="2"><?= $show_record['Contract'][0]['BillingDestination'][0]['name'] ?></td>
                </tr>
                <tr>
                  <td>住所</td>
                  <td colspan="2"><?= $show_record['Contract'][0]['BillingDestination'][0]['address'] ?></td>
                </tr>
                <tr>
                  <td>電話</td>
                  <td colspan="2"><?= $show_record['Contract'][0]['BillingDestination'][0]['tel'] ?></td>
                </tr>
              </tbody>
            </table>
          <? } ?>

          <h4>ご契約内容</h4>
          <table class="table table-bordered table-condensed table_color">
            <tbody>
              <tr>
                <td>ご契約日</td>
                <td><?= $show_record['Contract'][0]['contract_date'] ?></td>
              </tr>
              <tr>
                <td><input type="checkbox" name="contract_target" onclick="return false" value=0 <? if((int) $show_record['Contract'][0]['target'] === 0) { echo 'checked'; } ?>>企業</td>
                <td><input type="checkbox" name="contract_target" onclick="return false" value=1 <? if((int) $show_record['Contract'][0]['target'] === 1) { echo 'checked'; } ?>>店舗</td>
              </tr>
              <tr>
                <td>商材名</td>
                <td><?= $show_record['Contract'][0]['Product']['name'] ?></td>
              </tr>
              <tr>
                <td>初期費用</td>
                <td><?= $show_record['Contract'][0]['initial_cost'] ?>&nbsp;円</td>
              </tr>
              <tr>
                <td><input type="checkbox" name="period_flag" onclick="return false" value=0 <? if((int) $show_record['Contract'][0]['period_flag'] === 0) { echo 'checked'; } ?>>年間</td>
                <td><input type="checkbox" name="period_flag" onclick="return false" value=1 <? if((int) $show_record['Contract'][0]['period_flag'] === 1) { echo 'checked'; } ?>>月額</td>
              </tr>
              <tr>
                <td>金額</td>
                <td><?= $show_record['Contract'][0]['payment'] ?>&nbsp;円</td>
              </tr>
              <tr>
                <td>契約期間</td>
                <td><?= $show_record['Contract'][0]['start_date'] . '〜' . $show_record['Contract'][0]['end_date'] ?></td>
              </tr>
              <tr>
                <td><?= $show_record['Contract'][0]['initial_cost'] ?>&nbsp;円</td>
                <td colspan="2"><?= $show_record['Contract'][0]['detail'] ?></td>
              </tr>
            </tbody>

          </table>
        <? } ?>

        <? if ($account_record['authority_group_id'] == 1) { ?>
        <form class="once_submit" method="POST" action="<?= url_for('client/deleteRecordSubmit') ?>" onclick='return confirm("この顧客情報を削除します。本当によろしいですか？");'>
          <input type="hidden" name="store_id" value="<?= $store_records[$list_num]['id'] ?>">
          <button type="submit" class="btn btn-default">レコード削除</button>
        </form>
        <? } ?>
        
      </div>
    </div>



  </div><!-- /#page-wrapper -->

</div><!-- /#wrapper -->

<script>
$(function(){
  $('.once_submit').disableOnSubmit();
});
</script>
