<div id="wrapper">

  <!-- Sidebar -->
  <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="<?= url_for('client/show?list_num=' . $list_num) ?>">顧客管理ページ</a>
    </div>

    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse navbar-ex1-collapse">
      <ul class="nav navbar-nav side-nav">
        <div align="center">
          <? if ($pager_id > 1) { ?>
            <a href="<?= url_for('client_show', array('list_num' => 0, 'pager_id' => $pager_id - 1)) ?>">前へ</a>
          <? } else { ?>
            <a>前へ</a>
          <? } ?>
          <span><font color="white">&nbsp;<?= $pager_id ?> /<?= $max_pager_id ?>&nbsp;</font></span>
          <? if ($next_flag) { ?>
            <a href="<?= url_for('client_show', array('list_num' => 0, 'pager_id' => $pager_id + 1)) ?>">次へ</a>
          <? } else { ?>
            <a>次へ</a>
          <? } ?>
        </div>
        <? $num = 0; ?>
        <? foreach($store_records as $store_record ) { ?>
          <? if($num == $list_num) { ?>
            <li class="active">
              <a href="<?= url_for('client_show', array('list_num' => $num, 'pager_id' => $pager_id)) ?>">
                <i class="fa fa-dashboard"></i>
                <?= $store_record['company_name'] ?>
              </a>
            </li>
          <? } else { ?>
            <li>
              <a href="<?= url_for('client_show', array('list_num' => $num, 'pager_id' => $pager_id)) ?>">
                <i class="fa fa-dashboard"></i>
                <?= $store_record['company_name'] ?>
              </a>
            </li>
          <? } ?>
          <? $num++; ?>
        <? } ?>
      </ul>

      <ul class="nav navbar-nav navbar-right navbar-user">
        <li class="dropdown messages-dropdown">
          <a href="<?= url_for('client/show') ?>" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-envelope"></i> 企業詳細ページ </b></a>
        </li>
        <li class="dropdown messages-dropdown">
          <a href="<?= url_for('client/search') ?>" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-envelope"></i> 検索ページ </b></a>
        </li>
        <li class="dropdown messages-dropdown">
          <a href="<?= url_for('client/new') ?>" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-envelope"></i> 新規企業登録 </b></a>
        </li>
        <li class="dropdown user-dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-user"></i> Account <b class="caret"></b></a>
          <ul class="dropdown-menu">
            <li><a href="#"><i class="fa fa-user"></i> Profile</a></li>
            <li><a href="#"><i class="fa fa-envelope"></i> Inbox <span class="badge">7</span></a></li>
            <li><a href="#"><i class="fa fa-gear"></i> Settings</a></li>
            <li class="divider"></li>
            <li><a href="#"><i class="fa fa-power-off"></i> Log Out</a></li>
          </ul>
        </li>
      </ul>
    </div><!-- /.navbar-collapse -->
  </nav>

  <div id="page-wrapper" class="page_bg_color">

    <div class="row">
      <div class="col-lg-12">
      </div>
    </div>

    <div class="row">
    
      <div class="col-md-6">
        <form class="once_submit" method="POST" action="<?= url_for('client/changeSubmit') ?>">
          <input type="hidden" name="store_id" value="<?= $store_records[$list_num]['id'] ?>">
          <h4>企業・店舗情報&nbsp;&nbsp;<button type="submit" class="btn btn-default">保存</button>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            <? if($list_num > 0) { ?>
              <button type="button" onclick="location.href='<?= url_for('client_show', array('list_num' => $list_num - 1, 'pager_id' => $pager_id)) ?>'" class="btn btn-default">前のページへ</button>
            <? } else { ?>
              <? if($pager_id > 1) { ?>
                <button type="button" onclick="location.href='<?= url_for('client_show', array('list_num' => $per_page - 1, 'pager_id' => $pager_id - 1)) ?>'" class="btn btn-default">前のページへ</button>
              <? } else { ?>
                <button type="button" onclick="location.href='<?= url_for('client_show', array('list_num' => $list_num, 'pager_id' => $pager_id)) ?>'" class="btn btn-default">前のページへ</button>
              <? } ?>
            <? } ?>
            <? if($list_num < count($store_records) - 1) { ?>
              &nbsp;&nbsp;<button type="button" onclick="location.href='<?= url_for('client_show', array('list_num' => $list_num + 1, 'pager_id' => $pager_id)) ?>'" class="btn btn-default">次のページへ</button>
            <? } else { ?>
              <? if($pager_id < $max_pager_id) { ?>
                &nbsp;&nbsp;<button type="button" onclick="location.href='<?= url_for('client_show', array('list_num' => 0, 'pager_id' => $pager_id + 1)) ?>'" class="btn btn-default">次のページへ</button>
              <? } else { ?>
                &nbsp;&nbsp;<button type="button" onclick="location.href='<?= url_for('client_show', array('list_num' => $list_num, 'pager_id' => $pager_id)) ?>'" class="btn btn-default">次のページへ</button>
              <? } ?>
            <? } ?>
          </h4>
          <table class="table table-bordered table-condensed table_color">
            <tbody>
              <tr>
                <? if($store_records[$list_num]['new_flag'] == true) { ?>
                  <td><input type="radio" name="new_flag" value=1 checked>新規</td>
                  <td colspan="3"><input type="radio" name="new_flag" value=1 disabled>既存</td>
                <? } else { ?>
                  <td><input type="radio" name="new_flag" value=0 disabled>新規</td>
                  <td colspan="3"><input type="radio" name="new_flag" value=0 checked>既存</td>
                <? } ?>
              </tr>
              <tr>
                <td><input type="checkbox" name="call_again_flag" value=1 <? if($store_records[$list_num]['call_again_flag'] == true) { echo 'checked'; } ?>>再コールチェック</td>
                <td>再コール時間</td>
                <? list($year, $month, $day, $hour, $min, $sec) = preg_split("/[-: ]/", $store_records[$list_num]['call_schedule']); ?>
                <td colspan="2"><? include_partial('datetimeExistForm', array('name' => 'call', 'year' => $year, 'month' => $month, 'day' => $day, 'hour' => $hour, 'min' => $min)) ?></td>
              </tr>
              <tr>
                <td><input type="checkbox" onclick="return false" name="failure_receive_flag" value=1 <? if($store_records[$list_num]['failure_receive_flag'] == true) { echo 'checked'; } ?>>失注</td>
                <td colspan="3"><input type="checkbox" onclick="return false" name="phone_ban_flag" value=1 <? if($store_records[$list_num]['phone_ban_flag'] == true) { echo 'checked'; } ?>>コール禁止</td>
              </tr>
            </tbody>
          </table>
    
          <table class="table table-bordered table-condensed table_color">
            <tbody>
              <tr>
                <td class="cell_color">企業名</td>
                <td colspan="3"><?= $store_records[$list_num]['company_name'] ?></td>
              </tr>
              <tr>
                <td class="cell_color">代表者名</td>
                <td><?= $store_records[$list_num]['president'] ?></td>
                <td class="cell_color">先方担当者名</td>
                <td><input type="text" name="person_in_charge" value="<?= $store_records[$list_num]['person_in_charge'] ?>"></td>
              </tr>
              <tr>
                <td class="cell_color">営業担当者名</td>
                <td colspan="3"><?= $store_records[$list_num]['sales_representative'] ?></td>
              </tr>
              <tr>
                <td class="cell_color">企業住所</td>
                <td colspan="3"><?= $store_records[$list_num]['company_address'] ?></td>
              </tr>
              <tr>
                <td class="cell_color">企業TEL</td>
                <td><?= $store_records[$list_num]['company_tel'] ?></td>
                <td class="cell_color">企業FAX</td>
                <td><?= $store_records[$list_num]['company_fax'] ?></td>
              </tr>
              <tr>
                <td class="cell_color">店舗名</td>
                <td colspan="3"><?= $store_records[$list_num]['store_name'] ?></td>
              </tr>
              <tr>
                <td class="cell_color">店舗住所</td>
                <td colspan="3"><?= $store_records[$list_num]['store_address'] ?></td>
              </tr>
              <tr>
                <td class="cell_color">店舗TEL</td>
                <td><?= $store_records[$list_num]['store_tel'] ?></td>
                <td class="cell_color">店舗FAX</td>
                <td><?= $store_records[$list_num]['store_fax'] ?></td>
              </tr>
              <tr>
                <td class="cell_color">地方</td>
                <td><?= $store_records[$list_num]['region'] ?></td>
                <td class="cell_color">都道府県</td>
                <td><?= $store_records[$list_num]['prefecture'] ?></td>
              </tr>
              <tr>
                <td class="cell_color">メールアドレス</td>
                <td colspan="3"><input type="text" name="mail" style="width:100%" value="<?= $store_records[$list_num]['mail'] ?>"></td>
              </tr>
              <tr>
                <td class="cell_color">URL</td>
                <td colspan="3"><input type="text" name="url" style="width:100%" value="<?= $store_records[$list_num]['url'] ?>"></td>
              </tr>
              <tr>
                <td class="cell_color">業種ジャンル</td>
                <td><?= $store_records[$list_num]['genre'] ?></td>
                <td class="cell_color">店舗数</td>
                <td></td>
              </tr>
              <tr>
                <td colspan="4">
                  <textarea name="memo" style="width:100%" rows="3"><?= $store_records[$list_num]['memo'] ?></textarea>
                </td>
              </tr>
            </tbody>
          </table>
        </form>
    
        <h4>提案商材</h4>
        <table class="table table-bordered table-condensed table_color">
          <tbody>
            <? //FIXME: 強引に行っているので後で処理を修正 ?>
            <? $pp_gochipon = array(); ?>
            <? $pp_application = array(); ?>
            <? $pp_addnet = array(); ?>
            <? foreach($store_records[$list_num]['ProposalProduct'] as $atom) { ?>
              <?
                switch ($atom['Product']['name']) {
                  case 'ごちぽん':
                    $pp_gochipon = $atom;
                    break;
                  case 'アプリ':
                    $pp_application = $atom;
                    break;
                  case 'アドネットワーク広告':
                    $pp_addnet = $atom;
                    break;
                }
              ?>
            <? } ?>
            <tr>
              <? if (isset($pp_gochipon['id'])) { ?>
                <td><input type="checkbox" onclick="return false" name="pp_gochipon" value=1 checked>ごちぽん</td>
                <td><?= $pp_gochipon['region'] ?></td>
                <td><?= $pp_gochipon['prefecture'] ?></td>
              <? } else { ?>
                <td><input type="checkbox" onclick="return false" name="pp_gochipon" value=1 >ごちぽん</td>
                <td></td>
                <td></td>
              <? } ?>
            </tr>
            <tr>
              <? if (isset($pp_application['id'])) { ?>
                <td colspan="3"><input type="checkbox" onclick="return false" name="pp_application" value=1 checked>アプリ</td>
              <? } else { ?>
                <td colspan="3"><input type="checkbox" onclick="return false" name="pp_application" value=1>アプリ</td>
              <? } ?>
            </tr>
            <tr>
              <? if (isset($pp_addnet['id'])) { ?>
                <td colspan="3"><input type="checkbox" onclick="return false" name="pp_addnet" value=1 checked>アドネットワーク広告</td>
              <? } else { ?>
                <td colspan="3"><input type="checkbox" onclick="return false" name="pp_addnet" value=1>アドネットワーク広告</td>
              <? } ?>
            </tr>
          </tbody>
        </table>
      </div>
    
      <div class="col-md-6">
        <h4>コール一覧</h4>
        <div class="call_scroll call_bg_color">
          <? foreach($store_records[$list_num]['CallInfo'] as $atom) { ?>
            <form class="once_submit" method="POST" action="<?= url_for('client/callChangeSubmit') ?>">
              <table class="table table-bordered table-condensed table_color">
                <input type="hidden" name="id" value="<?= $atom['id'] ?>">
                <tbody>
                  <tr>
                    <td>コール日時</td>
                    <td><?= $atom['call_date'] ?></td>
                  </tr>
                  <tr>
                    <td><input type="checkbox" name="call_target" value="0" <? if((int) $atom['target'] === 0) { echo 'checked'; } ?>>企業</td>
                    <td><input type="checkbox" name="call_target" value="1" <? if((int) $atom['target'] === 1) { echo 'checked'; } ?>>店舗</td>
                  </tr>
                  <tr>
                    <td colspan="2">コール内容詳細</td>
                  </tr>
                  <tr>
                    <td colspan="2"><textarea name="call_detail" style="width:100%"><?= $atom['detail'] ?></textarea><button type="submit" class="btn btn-default">変更</button></td>
                  </tr>
                </tbody>
              </table>
            </form>
          <? } ?>
          <form class="once_submit" method="POST" action="<?= url_for('client/callAddSubmit') ?>">
            <input type="hidden" name="store_id" value="<?= $store_records[$list_num]['id'] ?>">
            <table class="table table-bordered table-condensed table_color">
              <tbody>
                <tr>
                  <td><input type="radio" name="call_target" value="0" checked>企業</td>
                  <td><input type="radio" name="call_target" value="1">店舗</td>
                </tr>
                <tr>
                  <td colspan="2">コール内容詳細</td>
                </tr>
                <tr>
                  <td colspan="2"><textarea name="call_detail" style="width:100%" rows="3"></textarea><button type="submit" class="btn btn-default">追加</button></td>
                </tr>
              </tbody>
            </table>
          </form>
        </div>

        <h4>取引商材（ご契約済み）</h4>
        <table class="table table-bordered table-condensed table_color">
          <tbody>
            <? //FIXME: 強引に行っているので後で処理を修正 ?>
            <? $dp_gochipon = array(); ?>
            <? $dp_application = array(); ?>
            <? $dp_addnet = array(); ?>
            <? foreach($store_records[$list_num]['DealProduct'] as $atom) { ?>
              <?
                switch ($atom['Product']['name']) {
                  case 'ごちぽん':
                    $dp_gochipon = $atom;
                    break;
                  case 'アプリ':
                    $dp_application = $atom;
                    break;
                  case 'アドネットワーク広告':
                    $dp_addnet = $atom;
                    break;
                }
              ?>
            <? } ?>
            <tr>
              <? if (isset($dp_gochipon['id'])) { ?>
                <td><input type="checkbox" onclick="return false" name="dp_gochipon" value=1 checked>ごちぽん</td>
                <td><?= $dp_gochipon['region'] ?></td>
                <td><?= $dp_gochipon['prefecture'] ?></td>
              <? } else { ?>
                <td><input type="checkbox" onclick="return false" name="dp_gochipon" value=1 >ごちぽん</td>
                <td></td>
                <td></td>
              <? } ?>
            </tr>
            <tr>
              <? if (isset($dp_application['id'])) { ?>
                <td colspan="3"><input type="checkbox" onclick="return false" name="dp_application" value=1 checked>アプリ</td>
              <? } else { ?>
                <td colspan="3"><input type="checkbox" onclick="return false" name="dp_application" value=1>アプリ</td>
              <? } ?>
            </tr>
            <tr>
              <? if (isset($dp_addnet['id'])) { ?>
                <td colspan="3"><input type="checkbox" onclick="return false" name="dp_addnet" value=1 checked>アドネットワーク広告</td>
              <? } else { ?>
                <td colspan="3"><input type="checkbox" onclick="return false" name="dp_addnet" value=1>アドネットワーク広告</td>
              <? } ?>
            </tr>
          </tbody>
        </table>
    
        <? if(isset($store_records[$list_num]['Contract'][0])) { ?>
          <? if(isset($store_records[$list_num]['Contract'][0]['BillingDestination'][0])) { ?>
            <h4>請求書先</h4>
            <table class="table table-bordered table-condensed table_color">
              <tbody>
                <tr>
                  <td><input type="checkbox" onclick="return false" name="example" value="サンプル" <? if((int) $store_records[$list_num]['Contract'][0]['BillingDestination'][0]['target'] === 0) { echo 'checked'; } ?>>企業</td>
                  <td><input type="checkbox" onclick="return false" name="example" value="サンプル" <? if((int) $store_records[$list_num]['Contract'][0]['BillingDestination'][0]['target'] === 1) { echo 'checked'; } ?>>店舗</td>
                  <td><input type="checkbox" onclick="return false" name="example" value="サンプル" <? if((int) $store_records[$list_num]['Contract'][0]['BillingDestination'][0]['target'] === 2) { echo 'checked'; } ?>>その他</td>
                </tr>
                <tr>
                  <td>宛名</td>
                  <td colspan="2"><?= $store_records[$list_num]['Contract'][0]['BillingDestination'][0]['name'] ?></td>
                </tr>
                <tr>
                  <td>住所</td>
                  <td colspan="2"><?= $store_records[$list_num]['Contract'][0]['BillingDestination'][0]['address'] ?></td>
                </tr>
                <tr>
                  <td>電話</td>
                  <td colspan="2"><?= $store_records[$list_num]['Contract'][0]['BillingDestination'][0]['tel'] ?></td>
                </tr>
              </tbody>
            </table>
          <? } ?>

          <h4>ご契約内容</h4>
          <table class="table table-bordered table-condensed table_color">
            <tbody>
              <tr>
                <td>ご契約日</td>
                <td><?= $store_records[$list_num]['Contract'][0]['contract_date'] ?></td>
              </tr>
              <tr>
                <td><input type="checkbox" name="contract_target" onclick="return false" value=0 <? if((int) $store_records[$list_num]['Contract'][0]['target'] === 0) { echo 'checked'; } ?>>企業</td>
                <td><input type="checkbox" name="contract_target" onclick="return false" value=1 <? if((int) $store_records[$list_num]['Contract'][0]['target'] === 1) { echo 'checked'; } ?>>店舗</td>
              </tr>
              <tr>
                <td>商材名</td>
                <td><?= $store_records[$list_num]['Contract'][0]['Product']['name'] ?></td>
              </tr>
              <tr>
                <td>初期費用</td>
                <td><?= $store_records[$list_num]['Contract'][0]['initial_cost'] ?>&nbsp;円</td>
              </tr>
              <tr>
                <td><input type="checkbox" name="period_flag" onclick="return false" value=0 <? if((int) $store_records[$list_num]['Contract'][0]['period_flag'] === 0) { echo 'checked'; } ?>>年間</td>
                <td><input type="checkbox" name="period_flag" onclick="return false" value=1 <? if((int) $store_records[$list_num]['Contract'][0]['period_flag'] === 1) { echo 'checked'; } ?>>月額</td>
              </tr>
              <tr>
                <td>金額</td>
                <td><?= $store_records[$list_num]['Contract'][0]['payment'] ?>&nbsp;円</td>
              </tr>
              <tr>
                <td>契約期間</td>
                <td><?= $store_records[$list_num]['Contract'][0]['start_date'] . '〜' . $store_records[$list_num]['Contract'][0]['end_date'] ?></td>
              </tr>
              <tr>
                <td><?= $store_records[$list_num]['Contract'][0]['initial_cost'] ?>&nbsp;円</td>
                <td colspan="2"><?= $store_records[$list_num]['Contract'][0]['detail'] ?></td>
              </tr>
            </tbody>
          </table>
        <? } ?>
    
      </div>
    </div>



  </div><!-- /#page-wrapper -->

</div><!-- /#wrapper -->

<script>
$(function(){
  $('.once_submit').disableOnSubmit();
});
</script>
