<?php

/**
 * top actions.
 *
 * @package    management
 * @subpackage top
 * @author     Your name here
 * @version    SVN: $Id: actions.class.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class topActions extends sfActions
{

  /**
   * ログインページ
   */
  public function executeLogin(sfWebRequest $request)
  {
    if ($request->isMethod('post')) {
      $post_data = $request->getPostParameters();

      $account_record = AccountTable::getInstance()->findOneByName($post_data['user_id']);

      if (crypt($post_data['user_pass'], $account_record['password']) === $account_record['password']) {
        $this->getUser()->login($account_record);
      }

    }

    if ($this->getuser()->isAuthenticated()) {
      $this->redirect('client/show');
    }
  }
  
  /**
   * ログアウト処理
   */
  public function executeLogoutSubmit(sfWebRequest $request)
  {
    $this->getUser()->logout();
    $this->redirect('top/login');
  }


}
