<?php

/**
 *
 * @package    adptMobileAuthPlugin
 * @subpackage actions
 * @author     yutaka ozaki <y_ozaki@adappt.jp>
 */

/**
 * アクションクラス
 *
 * アクションクラス全体に反映させる機能
 */
class BaseActions extends sfActions
{

  /**
   * プレアクション
   *
   * アクションの処理が走る前にcssとlayoutをセット
   */
  public function preExecute()
  {
    
  }

  /**
   * 指定されたモジュール、アクションにフォワード
   *
   * @param string $module モジュール名、指定がない場合は現在のモジュール
   * @param string $action アクション名、指定がない場合は現在のアクション
   */
  public function forward($module, $action)
  {

    if (!$module) {
      $module = $this->getModuleName();
    }

    if (!$action) {
      $action = $this->getActionName();
    }

    return parent::forward($module, $action);
  }

  /**
   * 404ページにフォワード
   *
   * @param string $message 未実装
   * @return
   */
  public function forward404($message = null)
  {
    return $this->forward('adpt', '404');
    parent::forward404();
  }

  /**
   * 何かエラーが起きた時に飛ばす共通ページ
   */
  public function forwardError()
  {
    return $this->forward('adpt', 'error');
  }

}
